<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--bootstrap-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!--end bootstrap-->
    <link rel="icon" href="{{ asset('images/logo.png') }}" type="image/x-icon">
    <!--owl.carousel-->
    <link rel="stylesheet" href="./owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="./owlcarousel/assets/owl.theme.default.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="owlcarousel/owl.carousel.min.js"></script>
    <!--end owl.carousel-->

    <!--chosen-->
    <link href="{{ asset('/chosen/chosen.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('/chosen/chosen.jquery.min.js')}}" type="text/javascript"></script>
    <!--end chosen-->

    <!--ckediter-->
    <script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
    <!--end ckediter-->
    <link rel="stylesheet" href="{{ asset('/css/detail_story.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/login_register.css') }}">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/rating/starrr.css') }}">
    @if(empty(auth()->user()))
    <link rel="stylesheet" href="{{ asset('/css/login_register.css') }}">
    @endif
    @yield('rating')

    <script src="{{ asset('/js/js.js') }}"></script>
    <title>Detail Story</title>
</head>
@extends('layouts.guess')


@section('body')

{{-- test  --}}
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Báo Cáo Truyện</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="{{route('report')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label class="control-label" for="Name">Tên Truyện</label>
                        <input readonly type="text" class="form-control" id="exampleInputEmail1"
                            aria-describedby="emailHelp" placeholder="Tên Truyện" value="{{ $detail_story->name }}">

                        <input readonly type="hidden" class="form-control" id="exampleInputEmail1"
                            aria-describedby="emailHelp" placeholder="story_id" name="story_id"
                            value="{{$detail_story->id}}">
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <span>Lí Do</span>
                        </div>
                        <div>
                            <select name="content" data-placeholder="Chọn Lí Do..." class="chosen-select">
                                <option value=""></option>
                                <option value="Truyện Có Từ Ngữ Phản Cảm">Truyện Có Từ Ngữ Phản Cảm</option>
                                <option value="Truyện Có Hình Ảnh Phản Cảm">Truyện Có Hình Ảnh Phản Cảm</option>
                                <option value="Truyện Đạo Nhái">Truyện Đạo Nhái</option>
                                <option value="Truyện Ngược Đãi Động Vật">Truyện Ngược Đãi Động Vật</option>
                                <option value="Truyện Ngược Đãi Trẻ Em">Truyện Ngược Đãi Trẻ Em</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">

                        <label class="control-label" for="Name">Lí Do Khác</label>

                        <input name="different_content" type="text" class="form-control" id="exampleInputEmail1"
                            aria-describedby="emailHelp" placeholder="Lí Do Khác">

                        <div>

                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Báo Cáo</button>
                </form>
            </div>

            <!-- Modal footer -->
            {{-- <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
        </div> --}}

        </div>
    </div>
</div>
{{-- end test  --}}

<main class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="item-detail">
                    <div class="title-detail">
                        {{ $detail_story->name }}
                    </div>
                </div>
                <div class="detail-info">
                    <div class="row">
                        <div class="col-md-3 col-12">
                            <div class="image-cover" style="text-align: center">
                                <div style="display: inline-block">
                                    <img style="width: 180px; height: 260px;" src="{{ $detail_story->image }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <ul class="list-info">
                                <li class="author row">
                                    <p class="name col-md-4 col-4">
                                        <i class="fa fa-user"></i>
                                        Tác Giả:
                                    </p>
                                    <p class="col-md-8 col-8">
                                        <a
                                            href="{{ route('get-list-story-author', ['slug' => $detail_story->author_convert]) }}">
                                            {{ $detail_story->author }}</a>
                                    </p>
                                </li>
                                <li class="view row">
                                    <p class="name col-md-4 col-4">
                                        <i class="fa fa-eye"></i>
                                        Lượt Xem:
                                    </p>
                                    <p class="col-md-8 col-8">
                                        {{ $detail_story->view }}
                                    </p>
                                </li>
                                <li class="category row">
                                    <p class="name col-md-4 col-4">
                                        <i class="fa fa-tags"></i>
                                        Thể Loại
                                    </p>
                                    <p class="col-md-8 col-8">
                                        @foreach ( $detail_story->ListStoryCategory as $category )
                                        <a class="category-text"
                                            href="{{ route('get-list-story-catelogy',['slug'=>$category->name_convert]) }}">
                                            {{ $category->name }}
                                        </a>
                                        @endforeach
                                    </p>
                                </li>
                                <li class="state row">
                                    <p class="name col-md-4 col-4">
                                        <i class="fa fa-rss"></i>
                                        Tình Trạng:
                                    </p>
                                    <p class="col-md-8 col-8">
                                        @if( $detail_story->status_id == 1 )
                                        On going
                                        @endif
                                        @if ( $detail_story->status_id ==2 )
                                        Completed
                                        @endif
                                        @if ( $detail_story->status_id ==3 )
                                        Pause
                                        @endif
                                    </p>
                                </li>
                                <li class="date-update row">
                                    <p class="name col-md-4 col-4">
                                        <!-- <i class="fa fa-calendar-plus-o"></i> -->
                                        Ngày Đăng Truyện:
                                    </p>
                                    <p class="col-md-8 col-8">
                                        {{ $detail_story->created_at }}
                                    </p>
                                </li>
                                <li class="date-update-late row">
                                    <p class="name col-md-4 col-4">
                                        <!-- <i class="fa fa-calendar-minus-o"></i> -->
                                        Lần Cuối Cập Nhật:
                                    </p>
                                    <p class="col-md-8 col-8">
                                        {{ $detail_story->updated_at }}
                                    </p>
                                </li>
                                <li class="date-update-late row">
                                    <p class="name col-md-4 col-4">
                                        <!-- <i class="fa fa-calendar-minus-o"></i> -->
                                        Đánh Giá
                                    </p>
                                    <p class="col-md-8 col-8">
                                        {{ number_format($ratingAvg,1)}}/5.0 (Tổng Đánh Giá: {{ $rating->count() }})
                                    </p>
                                </li>
                                <li class="view row">
                                    <p class="name col-md-4 col-4">
                                        <i class="fa fa-heart"></i>
                                        Yêu Thích:
                                    </p>
                                    <p class="col-md-8 col-8">
                                        {{ $favourite->count() }}
                                    </p>
                                </li>
                            </ul>
                            <div class="row rating-favourite-button">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="interactive-button">
                    <div class="row">
                        <div class="col-md-4 col-4">
                            {{-- <div class="rating">
                                <p>Đánh Giá</p>
                                <div class='starrr' id='star1'></div>
                            </div> --}}
                            @if(!empty(auth()->user()))
                            @if($ratingUser)
                            <p>Bạn Đánh Giá {{$ratingUser->number_star}} <i class="fa fa-star"></i></p>
                            <div class="rating">
                                <p>Đánh Giá</p>
                                <div class='starrr' id='star1'></div>
                            </div>
                            <span class='your-choice-was' style='display: none;'>
                                {{-- Bạn Đang Chọn <span class='choice'></span> Sao --}}
                                <form method="post" id="formRate" action="{{ route('rate') }}">
                                    @csrf
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" id="number_star" name="number_star">
                                        <input type="hidden" class="form-control" id="rating_star" name="story_id"
                                            value="{{ $detail_story->id }}">
                                        <input type="hidden" class="form-control" id="rating_star" name="account_id"
                                            value="{{ auth()->user()->id }}">
                                    </div>
                                </form>
                            </span>
                            @else
                            <div class="rating">
                                <p>Đánh Giá</p>
                                <div class='starrr' id='star1'></div>
                            </div>
                            <span class='your-choice-was' style='display: none;'>
                                {{-- Bạn Đang Chọn <span class='choice'></span> Sao --}}
                                <form method="post" id="formRate" action="{{ route('rate') }}">
                                    @csrf
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" id="number_star" name="number_star">
                                        <input type="hidden" class="form-control" id="rating_star" name="story_id"
                                            value="{{ $detail_story->id }}">
                                        <input type="hidden" class="form-control" id="rating_star" name="account_id"
                                            value="{{ auth()->user()->id }}">
                                    </div>
                                </form>
                            </span>

                            @endif

                            @else
                            <div class="rating">
                                <p>Đánh Giá</p>
                                <div class='starrr' id='star1' onclick="submitFavoriteFail()"></div>
                            </div>
                            @endif

                        </div>
                        <div class="col-md-4 col-4">
                            <div class="favorite-button">

                                @if(!empty(auth()->user()))

                                @if($favouritedUser)
                                <form action="{{ route('favourite') }}" method="POST" class="form-inline"
                                    id="formFavorite">
                                    @csrf
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="story_id"
                                            value="{{ $detail_story->id }}">
                                        <input type="hidden" class="form-control" name="account_id"
                                            value="{{ auth()->user()->id }}">
                                    </div>
                                    <button class="favorite-link btn btn-danger" type="submit">
                                        <i class="fa fa-heart"></i>
                                        <span>Bỏ Yêu Thích</span>
                                    </button>
                                </form>
                                @else
                                <form action="{{ route('favourite') }}" method="POST" class="form-inline"
                                    id="formFavorite">
                                    @csrf
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="story_id"
                                            value="{{ $detail_story->id }}">
                                        <input type="hidden" class="form-control" name="account_id"
                                            value="{{ auth()->user()->id }}">
                                    </div>
                                    <button class="favorite-link btn btn-danger" type="submit">
                                        <i class="fa fa-heart"></i>
                                        <span>Yêu Thích</span>
                                    </button>
                                </form>
                                @endif
                                @else
                                <button class="favorite-link btn btn-danger" onclick="submitFavoriteFail()">
                                    <i class="fa fa-heart"></i>
                                    <span>Yêu Thích</span>
                                </button>
                                @endif

                            </div>
                        </div>
                        <div class="col-md-4 col-4">
                            @if(!empty(auth()->user()))
                            <button class="btn btn-warning" type="button" data-bs-toggle="modal"
                                data-bs-target="#myModal">
                                <i class="fa fa-flag"></i>
                                <span>Báo Cáo</span>
                            </button>
                            @else
                            <button class="btn btn-warning" type="button" onclick="submitFavoriteFail()">
                                <i class="fa fa-flag"></i>
                                <span>Báo Cáo</span>
                            </button>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="other-name">
                    <h3 class="tilte">
                        Tên Khác
                    </h3>
                    <div class="value">
                        {{ $detail_story->other_name }}
                    </div>
                </div>
                <div class="detail-content">
                    <h3 class="tilte">
                        Tóm Tắt
                    </h3>
                    <div class="value">
                        {!! $detail_story->description!!}
                    </div>
                </div>
                <hr>
                <!--list chapter-->
                <div class="list-chapter">
                    <h2>
                        <i class="fa fa-list"></i>
                        <span>Danh Sách Chương</span>
                    </h2>
                    <div class="load-chapter ">
                        <p>
                            <span class="title al-c">Tên Chương</span>
                            <span class="publishedDate al-c">Ngày Đăng</span>
                        </p>
                        <hr>
                        <!-- list chương -->
                        <div class="list-wrap">
                            @foreach($chapter as $chap)
                            <p class="chapter-info">
                                <span class="title">
                                    <a href="{{ route('read-chapter',['id'=>$chap->id]) }}">{{ $chap->name }}</a>
                                </span>
                                <span class="publishedDate">
                                    {{ $chap->created_at }}
                                </span>
                            </p>
                            @endforeach

                        </div>
                        <!--end list chuong-->
                    </div>
                </div>
                <!--end list chapter-->
                <div class="comment">
                    <div class="comment-box">
                        <div class="title-comment">
                            <h4>
                                Bình Luận ({{ $comment->count() }})
                            </h4>
                        </div>
                        <div class="row">
                            @if (empty(auth()->user()))
                            <span class="login-comment" style="padding:2%;"> Bạn Cần

                                <a href="{{ route('log-in') }}" style="color: #007bff;">Đăng Nhập</a>

                                Để Bình Luận
                            </span>
                            @else
                            <div class="avatar">
                                <img src="{{ auth()->user()->avatar}}" width="50px" height="50px">
                            </div>
                            <div class="text-panel">
                                <form>
                                    @csrf
                                    <div class="form-group">
                                        <input class="story-id" name="story_id" type="hidden"
                                            value="{{ $detail_story->id }}">
                                        <input type="hidden" class="account-id" name="account_id"
                                            value="{{ auth()->user()->id }}">
                                        {{-- <input  name="content" class="form-control comment" id="" aria-describedby="" placeholder="Bình Luận Tại Đây"> --}}
                                        <textarea id="content-comment" name="content" class="form-control comment"
                                            rows="1" cols="50" placeholder="Bình Luận Tại Đây"></textarea>
                                    </div>
                                    <div class="control-button">
                                        <button type="button" class="btn btn-success " id="control-button-comment">
                                            Đăng
                                        </button>
                                    </div>
                                </form>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="list-comment">
                        <div class="row">
                            @foreach ( $comment as $listcomment )
                            <div class="comment-showing">
                                @if(!empty(auth()->user()))
                                @if($listcomment->account_id==auth()->user()->id||auth()->user()->type_account_id==1)
                                <div class="action-admin-bar">
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-light dropdown-toggle"
                                            data-bs-toggle="dropdown">

                                        </button>
                                        <ul class="dropdown-menu">
                                            <li data-idcomment="{{ $listcomment->id }}"><a
                                                    class="dropdown-item editcomment">Chỉnh Sửa</a></li>
                                            <li data-idcomment="{{ $listcomment->id }}"><a
                                                    class="dropdown-item deletecomment">Xóa</a></li>

                                        </ul>
                                    </div>
                                </div>
                                @endif
                                @endif
                                <img class="avatar " src="{{ $listcomment->account->avatar }}" width="50px"
                                    height="50px">
                                <div class="text-panel">
                                    <div class="comment-name">
                                        {{ $listcomment->account->name }}
                                    </div>
                                    <div class="content-showing">
                                        <div id="comment-{{   $listcomment->id  }}">{{ $listcomment->content }}</div>

                                        <div class="button-control">
                                            {{ $listcomment->created_at }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 float-right">
                <div class="row top-group">
                    <div class="col-12 no-push push-3-m  no-push-l col-lg-12">
                        <section class="series-users">
                            <main>
                                <div class="series-owner group-mem">
                                    <img src="{{$user_upload->avatar}}" alt="Poster's avatar">
                                    <div class="series-owner-title">
                                        <span class="series-owner_name"><a
                                                href="{{ route('get-list-story-account',['id'=>$user_upload->id]) }}">{{ $user_upload->name }}</a></span>
                                    </div>
                                </div>
                            </main>
                        </section>
                    </div>
                </div>

                <section class="basic-section">
                    <header class="sect-header">
                        <span class="sect-title">Truyện Cùng Người Đăng</span>
                    </header>
                    <main class="d-lg-block">
                        <ul class="others-list">
                            @foreach($list_story_user as $list)
                            <li>
                                <div class="others-img no-padding">
                                    <div class="a6-ratio">
                                        <div class="content img-in-ratio">
                                            <img src="{{ $list->image }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="others-info">
                                    <h5 class="others-name">
                                        <a id="name-name"
                                            href="{{ route('get-detail-story',['id' => $list->id]) }}">{{$list->name}}</a>
                                    </h5>
                                    <small class="series-summary-8 description">{!!$list->description!!}</small>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </main>
                </section>


                <section class="basic-section">
                    <header class="sect-header">
                        <span class="sect-title">Truyện Nổi Bật</span>
                    </header>
                    <main class="d-lg-block">
                        <ul class="others-list">
                            @foreach ($list_story_outstanding as $list)

                            <li>
                                <div class="others-img no-padding">
                                    <div class="a6-ratio">
                                        <div class="content img-in-ratio">
                                            <img src="{{ $list->image }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="others-info">
                                    <h5 class="others-name"><a
                                            href="{{ route('get-detail-story',['id' => $list->id]) }}">{{ $list->name }}</a>
                                    </h5>
                                    <small class="series-summary-8 description">{!! $list->description!!}</small>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </main>
                </section>

            </div>
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Recipient:</label>
                                <input type="text" class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Message:</label>
                                <textarea class="form-control" id="message-text"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Send message</button>
                    </div>
                </div>
            </div>
        </div>

</main>

<script src="{{ asset('/rating/starrr.js')}}"></script>

<script>
$('#star1').starrr({
    change: function(e, value) {
        if (value) {
            $('.your-choice-was').show();
            $('.choice').text(value);
            $('#rating_star').show();
            $('#number_star').val(value);
            $('#formRate').submit();
        } else {
            $('.your-choice-was').hide();
        }
    }
});
</script>


<script>
function submitFavoriteFail() {
    alert('Bạn cần đăng nhập để sử dụng chức năng này!')
}
</script>
<script type="text/javascript">
CKEDITOR.replace('description_content');
</script>

{{-- edit Comment --}}
<script>
// $("input").prop('disabled', true);
// $("input").prop('disabled', false);
$(".editcomment").click(function() {

    var idcomment = $(this).parents().attr('data-idcomment');
    var temp = $("#comment-" + idcomment).html();
    $("#comment-" + idcomment).html("");
    var $input =
        `<input class="input-update-comment"  type="text" id="inputcomment-${idcomment}" value="${temp}"> <button class="btn btn-success" id="savecomment-${idcomment}">Cập Nhật</button>`;
    $("#comment-" + idcomment).html($input);
    saveComment(idcomment);

})
</script>
<script>
function saveComment(idcomment) {
    $("#savecomment-" + idcomment).click(function() {
        var formData = new FormData();
        formData.append('content', $('#inputcomment-' + idcomment).val());
        formData.append('id', idcomment);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "{{ route('save_comment') }}",
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
        }).done(function(res) {
            location.reload();
        })
    })

}
</script>

{{-- đăng bình luận --}}
<script>
$("#control-button-comment").click(function() {
    var formData = new FormData();
    formData.append('story_id', $('.story-id').val());
    formData.append('account_id', $('.account-id').val());
    formData.append('content', $('#content-comment').val());

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: "{{ route('comment-story') }}",
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
    }).done(function(res) {

        location.reload();
    })
})
</script>

<script>
// $("input").prop('disabled', true);
// $("input").prop('disabled', false);
$(".deletecomment").click(function() {

    var idcomment = $(this).parents().attr('data-idcomment');
    var formData = new FormData();
    formData.append('id', idcomment);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: "{{ route('delete-comment-story') }}",
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
    }).done(function(res) {

        location.reload();
    })

})
</script>


@endsection