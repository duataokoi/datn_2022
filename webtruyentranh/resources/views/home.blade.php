<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--bootstrap-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!--end bootstrap-->
    <link rel="icon" href="{{ asset('images/logo.png') }}" type="image/x-icon">
    <!--owl.carousel-->
    <link rel="stylesheet" href="{{ asset('/owlcarousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/owlcarousel/assets/owl.theme.default.min.css') }}">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="owlcarousel/owl.carousel.min.js"></script>
    <!--end owl.carousel-->
    <link rel="stylesheet" href="{{ asset('/css/home.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/login_register.css') }}">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/rating/starrr.css') }}">
    @if(empty(auth()->user()))
    <link rel="stylesheet" href="./css/login_register.css">
    @endif
    @yield('rating')

    <script src="./js/js.js"></script>
    <title>Home</title>
</head>

@extends('layouts.guess')

@section('body')

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="list-noi-bat">
                    <span class="top-tab_title title-active mt-4"><i class="fa fa-trophy"></i> Truyện Nổi Bật</span>
                    <!-- test slide show -->
                    <div class="owl-carousel owl-theme owl-loaded">
                        @foreach($outstanding as $value)
                        <div class="item">
                            <a href=" {{ route('get-detail-story',['id' => $value->id]) }}" data-bs-toggle="tooltip"
                                title="{{ $value->name }}"><img src="{{ $value->image }}" loading="lazy"></a>
                            <div class="title-story">
                                {{ $value->name }}
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <!--test slide show-->
                </div>
            </div>
            <div class="container pt-5">
                <div class="row mt-2">
                    <div class="col-md-9">
                        <section class="new-story-update">
                            <div class="title">
                                <span class="title-1"><i class="fa fa-hourglass-start"></i> Truyện</span>
                                <span class="title-2">Mới Cập Nhật</span>
                            </div>
                            <div class="list-story">
                                <main class="row">

                                    @foreach($new_chapter as $value)
                                    @if($value->story->deleted_at==NULL)
                                    <div class="thumb-item-flow col-6 col-md-3">
                                        <div class="thumb-wrapper">
                                            <a href="{{ route('get-detail-story',['id' => $value->story->id ]) }}"
                                                data-bs-toggle="tooltip" title="{{$value->story->name }}">
                                                <div class="a6-ratio">
                                                    <div class="img-in-ratio lazyloaded">
                                                        <img style="width: 100%; height: 80%; "
                                                            src="{{$value->story->image }}" alt="" loading="lazy">
                                                    </div>
                                                </div>
                                            </a>
                                            <div class="thumb-detail">
                                                <div class="thumb_attr chapter-title">
                                                    <a href="{{ route('read-chapter',['id'=>$value->id]) }}"
                                                        data-bs-toggle="tooltip"
                                                        title="{{$value->name}}">{{$value->name}}</a>
                                                </div>
                                                <div class="thumb_attr volume-title"></div>
                                            </div>
                                        </div>

                                        <div class="thumb_attr series-title">
                                            <a href="{{ route('get-detail-story',['id' => $value->story->id ]) }}"
                                                data-bs-toggle="tooltip"
                                                title="{{$value->story->name}}">{{$value->story->name}}</a>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach

                                    <div class="thumb-item-flow col-6 col-md-3">
                                        <div class="thumb-wrapper">
                                            <div class="a6-ratio">
                                                <div class="img-in-ratio lazyloaded">
                                                    <img style="width: 100%; height: 80%;"
                                                        src="./images/BiaTruyen/bia-nut-xem-them.jpg" alt=""
                                                        loading="lazy">
                                                </div>
                                            </div>
                                            <a href="{{ route('story-new-chapter') }}">
                                                <div class="thumb-see-more">
                                                    <div class="see-more-inside">
                                                        <div class="see-more-content">
                                                            <div class="see-more-icon">
                                                                <i class="fa fa-arrow-circle-right"></i>
                                                            </div>
                                                            <div class="see-more-text">
                                                                Xem Thêm
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </main>
                            </div>
                        </section>
                        <!-- truyen da hoan thanh -->

                        <section class="story-finish">
                            <div class="title">
                                <span class="title-1"><i class="fa fa-hourglass-start"></i> Truyện</span>
                                <span class="title-2">Đã hoàn thành</span>
                            </div>
                            <div class="list-story">
                                <main class="row">
                                    @foreach($complete as $value)
                                    <div class="thumb-item-flow col-6 col-md-3">
                                        <div class="thumb-wrapper">
                                            <a href="{{ route('get-detail-story',['id' => $value->story->id]) }}"
                                                data-bs-toggle="tooltip" title="{{$value->story->name}}">
                                                <div class="a6-ratio">
                                                    <div class="img-in-ratio lazyloaded">
                                                        <img style="width: 100%; height: 80%;" src="{{$value->story->image}}"
                                                            alt="" loading="lazy">
                                                    </div>
                                                </div>
                                            </a>
                                            <div class="thumb-detail">
                                                <div class="thumb_attr chapter-title">
                                                    <a href="#" data-bs-toggle="tooltip"
                                                        title="{{$value->name}}">{{$value->name}}</a>
                                                </div>
                                                <div class="thumb_attr volume-title"></div>
                                            </div>
                                        </div>
                                        <div class="thumb_attr series-title">
                                            <a href="{{ route('get-detail-story',['id' => $value->story->id]) }}" data-bs-toggle="tooltip"
                                                title="{{$value->story->name}}">{{$value->story->name}}</a>
                                        </div>
                                    </div>
                                    @endforeach


                                    <div class="thumb-item-flow col-6 col-md-3">
                                        <div class="thumb-wrapper">
                                            <div class="a6-ratio">
                                                <div class="img-in-ratio lazyloaded">
                                                    <img style="width: 100%; height: 80%;"
                                                        src="./images/BiaTruyen/bia-nut-xem-them.jpg" alt=""
                                                        loading="lazy">
                                                </div>
                                            </div>
                                            <a href="{{ route('story-completed') }}">
                                                <div class="thumb-see-more">
                                                    <div class="see-more-inside">
                                                        <div class="see-more-content">
                                                            <div class="see-more-icon">
                                                                <i class="fa fa-arrow-circle-right"></i>
                                                            </div>
                                                            <div class="see-more-text">
                                                                Xem Thêm
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </main>
                            </div>
                        </section>
                        <!--end truyen da hoan thanh-->
                        <section class="new-story">
                            <div class="title">
                                <span class="title-1"><i class="fa fa-hourglass-start"></i> Truyện</span>
                                <span class="title-2">Mới</span>
                            </div>
                            <div class="list-story-new">
                                <div class="row">

                                    @foreach($new_create as $value)
                                    <div class="list-item col-12 col-lg-6">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="series-cover">
                                                    <a href="{{ route('get-detail-story',['id' => $value->id]) }}">
                                                        <div class="a6-ratio">
                                                            <div class=" img-in-ratio lazyloaded">
                                                                <img style="width: 100%; height: 95% ;"
                                                                    src="{{$value->image}}" loading="lazy">
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="list-detail">
                                                    <h4 class="series-title">
                                                        <a
                                                            href="{{ route('get-detail-story',['id' => $value->id]) }}">{{$value->name}}</a>
                                                    </h4>
                                                    <div class="series-summary">
                                                        {!!$value->description!!}
                                                    </div>

                                                    <div class="lastest-chapter">
                                                        {{-- {{$value->name}} --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach


                                </div>
                                <div class="row">
                                    <a href="{{ route('story-new') }}">
                                        <div class="row">
                                        <button class="btn btn-outline-dark">
                                            <i class="fa fa-list">Xem Thêm</i>
                                        </button>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </section>
                    </div>

                    <div class="col-sm-3 col-12">
                        <section class="Wdgt">
                            <div class="title-favourite">
                                Top Yêu Thích Nhất
                            </div>
                            @foreach ( $top_favourite as $key => $list)
                            <ul class="list-story-favourite">
                                <li>
                                    <div class="TPost A">
                                        <a class="bookmark" href="">
                                            <span class="Top">
                                                #{{$key +1}}
                                                <i></i>
                                            </span>
                                            <div class="Image">
                                                <figure class="Objf TpMvPlay AAIco-play_arrow">
                                                    <a href="{{ route('get-detail-story',['id'=>$list->story->id]) }}">
                                                        <img src="{{ $list->story->image }}" width="55" height="85"
                                                            loading="lazy">
                                                    </a>
                                                </figure>
                                            </div>
                                            <div class="Title-story-favourite">
                                                <a href="{{ route('get-detail-story',['id'=>$list->story->id]) }}">
                                                    {{ $list->story->name }}
                                                </a>
                                            </div>
                                        </a>
                                        <p class="Info">
                                            <span class="Qlty">{{ $list->favourites }} Yêu Thích</span>
                                        </p>
                                    </div>
                                </li>
                            </ul>
                            @endforeach
                        </section>

                        <section class="basic-section">
                            <header class="sect-header">
                                <span class="sect-title">Đọc Gì Hôm Nay</span>
                            </header>
                            <main class="d-lg-block">
                                <ul class="others-list">
                                    @foreach($list_story_random as $list)
                                    <li>
                                        <div class="others-img no-padding">
                                            <div class="a6-ratio">
                                                <div class="content img-in-ratio">
                                                    <a href="{{ route('get-detail-story',['id' => $list->id]) }}"><img src="{{ $list->image }}"></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="others-info">
                                            <h6 class="others-name">
                                                <a id="name-name"
                                                    href="{{ route('get-detail-story',['id' => $list->id]) }}">{{$list->name}}</a>
                                            </h6>
                                            <small class="series-summary-8 description">{!!$list->description!!}</small>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </main>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection