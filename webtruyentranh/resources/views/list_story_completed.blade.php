<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--bootstrap-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!--end bootstrap-->
    <link rel="icon" href="{{ asset('images/logo.png') }}" type="image/x-icon">
    <!--owl.carousel-->
    <link rel="stylesheet" href="{{ asset('/owlcarousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/owlcarousel/assets/owl.theme.default.min.css') }}">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="owlcarousel/owl.carousel.min.js"></script>
    <!--end owl.carousel-->
    <link rel="stylesheet" href="{{ asset('/css/list_story.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/login_register.css') }}">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/rating/starrr.css') }}">
    @if(empty(auth()->user()))
    <link rel="stylesheet" href="{{ asset('/css/login_register.css') }}">
    @endif
    @yield('rating')

    <script src="{{ asset('/js/js.js') }}"></script>
    <title>List Story</title>
</head>

@extends('layouts.guess')

@section('body')
<main class="main">
    <div class="container ">
        <div class="row ">
            {{-- <div class="col-md-10 "> --}}
            <section class="basic-section">
                <p class="title-table">
                <div class="title-main">
                    <span class="title-1"><i class="fa fa-hourglass-start"></i>Danh Sách Theo</span>
                    <span class="title-2">Truyện Đã Hoàn Thành</span>
                </div>
                </p>
                <div class="list-story">
                    <main class="row">
                        @forelse ( $complete as $list)

                        <div class="thumb-item-flow col-md-3 col-6">
                            <div class="thumb-wrapper">
                                <a href="{{ route('get-detail-story',['id' => $list->story->id]) }}" data-bs-toggle="tooltip"
                                    title="{{ $list->story->name }}">
                                    <div class="a6-ratio">
                                        <div class="img-in-ratio lazyloaded">
                                            <img style="width: 100%; height: 90%;" src="{{ $list->story->image }}" alt=""
                                                loading="lazy">
                                        </div>
                                    </div>
                                </a>
                                <div class="thumb-detail">
                                    <div class="thumb_attr chapter-title">
                                        <a href="{{ route('read-chapter',['id'=>$list->id]) }}" data-bs-toggle="tooltip"
                                            title="{{ $list->name }}">{{ $list->name }}</a>
                                    </div>
                                    <div class="thumb_attr volume-title"></div>
                                </div>
                            </div>
                            <div class="thumb_attr series-title">
                                <a href="{{ route('get-detail-story',['id' => $list->story->id]) }}" data-bs-toggle="tooltip"
                                    title="Ore no Onna Tomodachi ga Saikou ni Kawaii">{{ $list->story->name }}</a>
                            </div>
                        </div>
                        @empty
                        <td>
                            Không Thấy Truyện Nào....
                        </td>
                        @endforelse

                    </main>
                </div>
            </section>
            <div class="text-center center-pagination">
                {{ $complete->appends(request()->all())->links() }}
            </div>

            {{-- </div> --}}

            {{-- <div class="col-md-2">
                <div class="image-cat-search">
                    <img src="{{ asset('images/cat_search.jpg') }}" width="200" height="200">
        </div>
    </div> --}}
    </div>
    </div>
</main>

@endsection