@extends('layouts.user')

@section('title', 'User manager')

@section('css', 'user.css')

@section('body')
<!-- danh chon -->
<section>
    <div class="content1">
        <div class="data-content">
            <div class="search-manager">
                <form autocomplete="off" class="d-flex" method="GET" accept-charset="UTF-8"
                    action="{{ route('search-story-disable') }}">
                    <input id="keywords" name="keywords" class="form-control me-2" type="text" placeholder="Tìm Truyện">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <div class="table-content">
                <p class="title-table">
                <div class="title-main">
                    <span class="title-1"><i class="fa fa-hourglass-start"></i>Truyện</span>
                    <span class="title-2">Đã Ẩn</span>
                    <!-- <span class="button-create">
                                <button class="btn btn-success">Thêm Truyện</button>
                            </span> -->
                </div>

                </p>
                <table class="table table-striped secondary table-bordered">
                    <thead>
                        <tr>
                            <th>Bìa Truyện</th>
                            <th>Tên Truyện</th>
                            <th>Lượt Xem</th>
                            <th>Thể Loại</th>
                            <th>Tình Trạng</th>
                            <th>Trạng Thái</th>
                            <th>Chức Năng</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($listStory as $story)
                        <tr>
                            <td><img src="{{ $story->image }}" height="150" loading="lazy"></td>
                            <td>
                                <a href="">{{ $story->name }}</a>

                            </td>
                            <td>{{ $story->view }} </td>

                            <td style="max-width: 200px">
                                @foreach ($story->ListStoryCategory as $list )
                                <span class='categogy-story'>

                                    {{ $list->name}}

                                </span>
                                @endforeach
                            </td>

                            @if($story->state == 1)
                            <td class="state-story">On going</td>
                            @endif

                            @if ($story->state == 2)
                            <td class="state-story">Completed</td>
                            @endif

                            @if($story->state == 3)
                            <td class="state-story">Pause</td>
                            @endif
                            <!--status-->
                            {{-- @if ($story->status_id==1)
                          <td class="status-enable">Hoạt Động </td>
                          @endif --}}

                            <td class="status-disable">Vô Hiệu Hóa </td>


                            <td>
                                {{-- <a href="{{ route('list-chapter', ['id' => $story->id]) }}" text-decoration="none">
                                <button type="button" class="btn btn-success" data-bs-toggle="tooltip"
                                    title="Danh Sách Chương">
                                    <i class="fa fa-list-ul"></i>
                                </button>
                                </a> --}}
                                <a href="{{ route('show-edit-story-disable',['id'=>$story->id]) }}">
                                    <button type="button" class="btn btn-primary" data-bs-toggle="tooltip"
                                        title="Sửa truyện">

                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                                {{-- <a href="{{ route('restore-story',['id'=>$story->id]) }}">
                                <button type="button" class="btn btn-danger" data-bs-toggle="tooltip"
                                    title="Khôi Phục truyện">

                                    <i class="fa fa-close"></i>
                                </button>
                                </a> --}}
                            </td>
                        </tr>
                        @empty
                        <p>Không có dữ liệu</p>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
</section>
</div>

@endsection