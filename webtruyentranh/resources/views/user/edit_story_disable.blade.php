@extends('layouts.user')

@section('title', 'Edit story')

@section('body')
<!-- danh chon -->
<section>
    <div class="content1">
        <div class="input-create-story">
            <div class="table-content">
                <p class="title-table">
                <div class="title-main">
                    <span class="title-1">Sửa</span>
                    <span class="title-2">Truyện Bị khóa</span>
                    <!-- <span class="button-create">
                                    <button class="btn btn-success">Thêm Truyện</button>
                                </span> -->
                </div>
                </p>
            </div>
            <div class="input-create-story">
                <form method="POST" action="{{route('edit-story-disable',['id' => $story->id])}}"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <div class="col-md-2">
                            <label class="control-label" for="Name">Tên Truyện</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="name" id="slug" onKeyUp="ChangeURL()"
                                aria-describedby="emailHelp" placeholder="Tên Truyện" value="{{ $story->name }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <label class="control-label" for="Name">Url Truyện</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" readonly="readonly" name="name_convert" id="convert_slug"
                                class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="Url Truyện" value="{{ $story->name_convert }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <label class="control-label" for="Name">Tên Khác</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="Tên Khác" name="other_name" value="{{ $story->other_name }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <label class="control-label" for="Name">Tác Giả</label>
                        </div>
                        <div class="col-md-10">

                            <input type="text" class="form-control" name="author" id="author" onKeyUp="ChangeAuthor()"
                                aria-describedby="emailHelp" placeholder="Tác Giả" value="{{ $story->author }}">
                            <input type="" readonly="readonly" name="author_convert" id="convert_author"
                                class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="Tác giả" value="{{ $story->author_convert }}">
                        </div>
                    </div>



                    <div class="form-group">
                        <label for="">Ảnh Bìa</label>
                        <input type="file" class="form-control" id="exampleFormControlFile1"
                            value=" {{ $story->image }}">
                    </div>
                    <img src="{{ $story->image }}" height="80">
                    <br>
                    <div class="form-group">
                        <div class="col-md-2">
                            <span>Thể Loại</span>
                            <span></span>
                        </div>
                        <div>
                            <select name="category[]" data-placeholder="Chọn Thể Loại..." class="chosen-select"
                                multiple>
                                @foreach ( $category as $listcategory )
                                @if($category_of_story->contains($listcategory->id))
                                <option value="{{ $listcategory->id }}" selected>{{ $listcategory->name }}</option>
                                @else
                                <option value="{{ $listcategory->id }}">{{ $listcategory->name }}</option>
                                @endif

                                @endforeach

                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <span>Mô Tả<span class="text-danger">*</span></span>
                        <textarea class="form-control" id="description_content" name="description" rows="5"
                            style="resize: none">{!! $story->description !!}</textarea>
                        @error('description')
                        <span class='text-danger' role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">
                            <label class="control-label" for="Name">Tình Trạng</label>
                        </div>
                        <div class="col-md-10">
                            <select name="state">
                                @if($story->state==1)
                                <option value="1" selected>On going</option>
                                <option value="2">Completed</option>
                                <option value="3">Pause</option>
                                @endif
                                @if($story->state==2)
                                <option value="1">On going</option>
                                <option value="2" selected>Completed</option>
                                <option value="3">Pause</option>
                                @endif
                                @if($story->state==3)
                                <option value="1">On going</option>
                                <option value="2">Completed</option>
                                <option value="3" selected>Pause</option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Cập Nhật Truyện</button>
                </form>
            </div>
        </div>
    </div>
</section>

<div class="" style="height: 300px;">

</div>
</div>
<!---->


{{-- <script>
        $(document).ready(function () {
        $('.chosen-select').chosen({width:"90%",no_results_text: "Không Tìm Thấy!"});
        });
        </script> --}}
<script src="{{ asset('/chosen/chosen.jquery.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
CKEDITOR.replace('description_content');
</script>


<script type="text/javascript">
function ChangeURL() {
    var slug;
    //Lấy text từ thẻ input title 
    slug = document.getElementById("slug").value;
    slug = slug.toLowerCase();
    //Đổi ký tự có dấu thành không dấu
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');
    //Xóa các ký tự đặt biệt
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    //Đổi khoảng trắng thành ký tự gạch ngang
    slug = slug.replace(/ /gi, "-");
    //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
    //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    //Xóa các ký tự gạch ngang ở đầu và cuối
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
    //In slug ra textbox có id “slug”
    document.getElementById('convert_slug').value = slug;
}
</script>



<script type="text/javascript">
function ChangeAuthor() {
    var slug;
    //Lấy text từ thẻ input title 
    slug = document.getElementById("author").value;
    slug = slug.toLowerCase();
    //Đổi ký tự có dấu thành không dấu
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');
    //Xóa các ký tự đặt biệt
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    //Đổi khoảng trắng thành ký tự gạch ngang
    slug = slug.replace(/ /gi, "-");
    //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
    //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    //Xóa các ký tự gạch ngang ở đầu và cuối
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
    //In slug ra textbox có id “slug”
    document.getElementById('convert_author').value = slug;
}
</script>


@endsection