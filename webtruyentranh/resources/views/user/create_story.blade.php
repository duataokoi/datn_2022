@extends('layouts.user')

@section('title', 'Create story')

@section('css', 'create_story.css')

@section('body')
<!-- danh chon -->
<section>
    <div class="content1">
        <div class="input-create-story">
            <div class="table-content">
                <p class="title-table">
                <div class="title-main">
                    <span class="title-1">Thêm Truyện</span>
                    <span class="title-2">Mới</span>
                </div>
                </p>
            </div>
            <div class="input-create-story">
                <form method="post" action="{{ route('add-story') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <div class="col-md-2">
                            <label class="control-label" for="Name">ID Truyện</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" readonly="readonly" class="form-control" name="id"
                                aria-describedby="emailHelp" placeholder="ID" value={{$finalId}}>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <label class="control-label" for="Name">Tên Truyện<span class="text-danger">*</span></label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="name" id="slug" onKeyUp="ChangeURL()"
                                aria-describedby="emailHelp" placeholder="Tên Truyện">
                            @error('name')
                            <span class='text-danger' role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <label class="control-label" for="Name">Url Truyện</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" readonly="readonly" name="name_convert" id="convert_slug"
                                class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="Url Truyện">
                            @error('name_convert')
                            <span class='text-danger' role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <label class="control-label" for="Name">Tên Khác<span class="text-danger">*</span></label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="other_name" id="exampleInputEmail1"
                                aria-describedby="emailHelp" placeholder="Tên Khác">
                            @error('other_name')
                            <span class='text-danger' role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <label class="control-label" for="Name">Tác Giả<span class="text-danger">*</span></label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="author" id="author" onKeyUp="ChangeAuthor()"
                                aria-describedby="emailHelp" placeholder="Tác Giả">
                            @error('author')
                            <span class='text-danger' role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <input type="hidden" readonly="readonly" name="author_convert" id="convert_author"
                                class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="Tác giả">
                            @error('author_convert')
                            <span class='text-danger' role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Ảnh Bìa<span class="text-danger">*</span></label>
                        <input type="file" class="form-control" name="image" id="image" accept="image/*" onchange="ImageFileAsURL()">
                        <div id="displayImg">
                            
                        </div>
                        @error('image')
                        <span class='text-danger' role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">
                            <span>Thể Loại<span class="text-danger">*</span></span>
                        </div>
                        <div>
                            <select name="category[]" data-placeholder="Chọn Thể Loại..." class="chosen-select"
                                multiple>
                                @foreach($category as $value)
                                <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach
                            </select>

                        </div>

                        @error('category')
                        <span class='text-danger' role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <span>Mô Tả<span class="text-danger">*</span></span>
                        <textarea class="form-control" id="description_content" name="description" rows="5"
                            style="resize: none"></textarea>
                        @error('description')
                        <span class='text-danger' role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">
                            <label class="control-label" for="Name">Tình Trạng</label>
                        </div>
                        <div class="col-md-10">
                            <select name="state">
                                <option value="1">On going</option>
                                <option value="2">Completed</option>
                                <option value="3">Pause</option>
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Thêm Truyện</button>
                </form>
            </div>
        </div>
    </div>
</section>

<div class="" style="height: 300px;">
</div>
</div>
<!---->

<script type="text/javascript">
CKEDITOR.replace('description_content');
</script>
<script type="text/javascript">
function ChangeURL() {
    var slug;
    //Lấy text từ thẻ input title 
    slug = document.getElementById("slug").value;
    slug = slug.toLowerCase();
    //Đổi ký tự có dấu thành không dấu
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');
    //Xóa các ký tự đặt biệt
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, ' ');
    //Đổi khoảng trắng thành ký tự gạch ngang
    slug = slug.replace(/ /gi, "-");
    //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
    //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    //Xóa các ký tự gạch ngang ở đầu và cuối
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
    //In slug ra textbox có id “slug”
    document.getElementById('convert_slug').value = slug;
}
</script>


<script type="text/javascript">
function ChangeAuthor() {
    var slug;
    //Lấy text từ thẻ input title 
    slug = document.getElementById("author").value;
    slug = slug.toLowerCase();
    //Đổi ký tự có dấu thành không dấu
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');
    //Xóa các ký tự đặt biệt
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    //Đổi khoảng trắng thành ký tự gạch ngang
    slug = slug.replace(/ /gi, "-");
    //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
    //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    //Xóa các ký tự gạch ngang ở đầu và cuối
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
    //In slug ra textbox có id “slug”
    document.getElementById('convert_author').value = slug;
}
</script>

<script type="text/javascript">
    function ImageFileAsURL(){
        var fileSelected = document.getElementById("image").files;
        if(fileSelected.length > 0){
            var fileToLoad = fileSelected[0];
            var fileReader = new FileReader();
            fileReader.onload = function (fileLoaderEvent){
                var srcData = fileLoaderEvent.target.result;
                var newImage = document.createElement('img');
                newImage.src = srcData;
                document.getElementById('displayImg').innerHTML = newImage.outerHTML;
            }
            fileReader.readAsDataURL(fileToLoad);
        }
    }
</script>

@endsection