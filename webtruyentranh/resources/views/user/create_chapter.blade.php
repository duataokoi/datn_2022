<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://unpkg.com/headroom.js"></script>
    <link rel="stylesheet" href="{{asset('/css/create_chapter.css')}}">
    <link rel="stylesheet" href="{{asset('/css/style.css')}}">
    <link rel="icon" href="{{ asset('images/logo.png') }}" type="image/x-icon">
    <title>Create Chapter</title>
</head>

<body>
    <div class="vc">
        <nav class="navbar navbar-expand-sm navbar bg-dark ">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ route('list-story-user')}}">Bảng Điều Khiển</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="mynavbar">
                    <ul class="navbar-nav me-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('home') }}"><i class="fa fa-fw fa-home"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('show-create-story')}}"><i class="fa fa-fw fa-pencil-square"></i>Thêm Truyện</a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">Quản Lí
                                Truyện</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('list-story-user') }}">Truyện Đã Đăng</a>
                                </li>
                                <li><a class="dropdown-item" href="{{ route('list-story-disable-user') }}">Truyện Đã
                                        Ẩn</a></li>

                            </ul>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="">
                <div class=" nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#"><img src="{{ auth()->user()->avatar }}" class="rounded-pill" height="50" width="50"></a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{route('user-info',['id' => auth()->user()->id])}}"><i class="fa fa-user"></i> Trang Cá Nhân</a></li>
                        @if(auth()->user()->type_account_id==2)
                        <li><a class="dropdown-item" href="{{ route('list-story-user') }}"><i class="fa fa-pencil"></i>
                                Quản Lí </a></li>
                        @else
                        <li><a class="dropdown-item" href="{{ route('list-story-admin') }}"><i class="fa fa-pencil"></i>
                                Quản Lí </a></li>
                        @endif
                        @if(auth()->user()->type_account_id==2)
                        <li><a class="dropdown-item" href="{{ route('get-list-story-favourite') }}"><i class="fa fa-heart"></i> Truyện Yêu Thích</a></li>
                        @endif
                        <li><a class="dropdown-item" href="{{ route('log-out') }}"><i class="fa fa-power-off"></i> Đăng
                                Xuất</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>


    <section>
        <div class="content1">
            <div class="input-create-story">
                <div class="table-content">
                    <p class="title-table">
                    <div class="title-main">
                        <span class="title-1">Thêm</span>
                        <span class="title-2">Chương</span>
                    </div>
                    </p>
                </div>
                <div class="input-create-story">
                    <form method="post" enctype="multipart/form-data" action="{{ route('add-chapter') }}">
                        @csrf
                        <input readonly type="hidden" name="story_id" class="form-control" value="{{$story->id}}">
                        <input readonly type="hidden" name="id" class="form-control" value="{{$finalId}}">
                        <input readonly type="hidden" name="folder" class="form-control" value="{{$story->name_convert}}">

                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label" for="Name">Tên Truyện</label>
                            </div>
                            <div class="col-md-10">
                                <input readonly type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tên Truyện" value="{{$story->name}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label" for="Name">Tên Chương <span class="text-danger">*</span></label>

                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="slug" name="name" onKeyUp="ChangeURL()" aria-describedby="emailHelp" placeholder="Tên Chương" value="">
                            </div>
                            @error('name')
                            <span class='text-danger' role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label" for="Name">Tên Url</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" readonly class="form-control" name="name_convert" id="convert_slug" aria-describedby="emailHelp" placeholder="Tên Url" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Ảnh <span class="text-danger">*</span></label>
                            <input type="file" class="form-control" id="exampleFormControlFile1" accept="image/*" name="image[]" multiple>
                            @error('image')
                            <span class='text-danger' role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Thêm Chương</button>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <script type="text/javascript">
        function ChangeURL() {
            var slug;
            //Lấy text từ thẻ input title 
            slug = document.getElementById("slug").value;
            slug = slug.toLowerCase();
            //Đổi ký tự có dấu thành không dấu
            slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
            slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
            slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
            slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
            slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
            slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
            slug = slug.replace(/đ/gi, 'd');
            //Xóa các ký tự đặt biệt
            slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, ' ');
            //Đổi khoảng trắng thành ký tự gạch ngang
            slug = slug.replace(/ /gi, "-");
            //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
            //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
            slug = slug.replace(/\-\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-/gi, '-');
            slug = slug.replace(/\-\-/gi, '-');
            //Xóa các ký tự gạch ngang ở đầu và cuối
            slug = '@' + slug + '@';
            slug = slug.replace(/\@\-|\-\@|\@/gi, '');
            //In slug ra textbox có id “slug”
            document.getElementById('convert_slug').value = slug;
        }
    </script>

</body>

</html>