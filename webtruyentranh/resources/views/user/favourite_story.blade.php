@extends('layouts.user')

@section('title', 'Favourite story')

@section('css', 'favourite_story.css')

@section('body')
<!-- danh chon -->
<section>
    <div class="content1">
        <div class="data-content">

            <div class="table-content">
                <p class="title-table">
                <div class="title-main">
                    <span class="title-1"><i class="fa fa-heart"></i>Truyện</span>
                    <span class="title-2">Yêu Thích</span>
                    <!-- <span class="button-create">
                                <button class="btn btn-success">Thêm Truyện</button>
                            </span> -->
                    <span>
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif
                    </span>
                </div>

                </p>
                <table class="table table-striped secondary table-bordered">
                    <thead>
                        <tr>
                            <th>Bìa Truyện</th>
                            <th>Tên Truyện</th>

                            <th>Chức Năng</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($listStory as $list )
                        <tr>
                            <td> <img src="{{ $list->story->image }}" height="120" loading="lazy"> </td>
                            <td><a
                                    href="{{ route('get-detail-story',['id'=>$list->story->id]) }}">{{ $list->story->name }}</a>
                            </td>


                            <td>
                                <a href="{{ route('delete-favourite-story',['id'=>$list->id]) }}">
                                    <button type="button" class="btn btn-outline-danger" data-bs-toggle="tooltip"
                                        title="xóa khỏi yêu thích">

                                        <i class="fa fa-heartbeat"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                        @empty
                        <td>Không có dữ liệu</td>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
</section>
</div>

@endsection