@extends('layouts.user')

@section('css', 'list_chapter.css')

@section('title', 'List chapter')


@section('body')

<main class="data-content">
    <div class="table-content">
        <p class="title-table">
        <div class="title-main">
            <span class="title-1"><i class="fa fa-list"></i>Danh Sách Chương Truyện:</span>
            <span class="title-2">{{ $story->name }}</span>
            <span class="button-create">
                <a href="{{ route('create-chapter', ['id' => $story->id]) }}">
                    <button class="btn btn-success">Thêm Chương</button>
                </a>
            </span>
        </div>

        <div class="search-manager">
            <form class="d-flex">
                <input class="form-control me-2" type="text" placeholder="Tìm Chương">
                <button class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
            </form>
        </div>

        </p>
        <table class="table table-striped secondary table-bordered">
            <thead>
                <tr>
                    <th>Tên Chương</th>
                    <th>Ngày Đăng</th>
                    <th>Cập Nhật Lần Cuối</th>
                    <th>Trạng thái</th>
                    <th>Chức Năng</th>
                </tr>
            </thead>
            <tbody>
                @forelse($chapter as $chap)
                <tr>
                    <td>{{ $chap->name }}</td>
                    <td>{{ $chap->created_at }}</td>
                    <td>{{ $chap->updated_at }}</td>
                    @if($chap->deleted_at == NULL)
                    <td class="status-enable">Hoạt Động </td>
                    @else
                    <td class="status-disable">Ngưng Hoạt Động </td>
                    @endif

                    <td><a href="{{route('show-edit-chapter', ['id' => $chap->id])}}"> <button type="button"
                                class="btn btn-primary" data-bs-toggle="tooltip" title="Sửa Chương">
                                <i class="fa fa-edit"></i>
                            </button>
                        </a>
                        @if($chap->deleted_at == NULL)
                        <a href="{{route('delete-chapter', ['id' => $chap->id])}}">
                            <button type="button" class="btn btn-danger" data-bs-toggle="tooltip" title="Ẩn Chương">

                                <i class="fa fa-trash"></i>
                            </button>
                        </a>
                        @else
                        <a href="{{route('restore-chapter', ['id' => $chap->id])}}">
                            <button type="button" class="btn btn-success" data-bs-toggle="tooltip"
                                title="Khôi phục Chương">
                                <i class="fa fa-check-square"></i>
                            </button>
                        </a>
                        @endif
                    </td>

                </tr>
                @empty
                <td>
                    Không Có Dữ Liệu
                </td>
                @endforelse
            </tbody>
        </table>
    </div>

</main>

@endsection