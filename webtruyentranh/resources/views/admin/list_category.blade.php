@extends('layouts.admin')

@section('title', 'Admin')

@section('body')
<!-- danh chon -->
<section>
    <div class="content1">
        <div class="data-content">
            <div class="search-manager">
                <form autocomplete="off" class="d-flex" method="GET" accept-charset="UTF-8"
                    action="">
                    <input id="keywords" name="keywords" class="form-control me-2" type="text" placeholder="Tìm Thể Loại">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <div class="table-content">
                        <p class="title-table">
                            <div class="title-main">
                                <span class="title-1"><i class="fa fa-hourglass-start"></i>Bảng</span>
                                <span class="title-2">Thể Loại</span>
                                <span class="button-create">
                                    <a href="{{ route('create-category') }}"><button class="btn btn-success">Thêm Thể Loại</button></a>
                                </span>
                            </div>
            
                            </p>
                    <table class="table table-striped secondary table-bordered">
                        <thead>
                            <tr>
                                <th>Tên Thể Loại</th>
                                <th>Tên Convert</th>
                                <th>Ngày Tạo</th>
                                <th>Lần Cuối Cập Nhật</th>
                                <th>Chức Năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $category as $list )
                            <tr>
                                
                                <td>{{ $list->name }}</td>
                                <td>{{ $list->name_convert }}</td>
                                <td>{{ $list->created_at }}</td>
                                <td>{{ $list->updated_at }}</td>
                                <td>
                                <a href="{{ route('show-edit-category',['id'=>$list->id]) }}">
                                    <button type="button" class="btn btn-success">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                                <a href="">
                                    <button type="button" class="btn btn-danger">
                                        <i class="fa fa-close"></i>
                                    </button>
                                </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
        </div>
</section>
<div class="text-center center-pagination">
    {{ $category->appends(request()->all())->links() }}
</div>
</div>

<!-- <div class="spinner-border text-primary" role="status">
        <span class="sr-only">Loading...</span>
      </div> -->
<!-- bootstrap -->
<footer style="background-color: #333; padding: 30px 10px;">
    <div class="container">
        <span class="right" style="color: #fff; font-weight: 700; margin-right: 20px; float: right;">Liên hệ: <a
                href="mailto:0306191423@caothang.edu.vn" target="_blank" style="color: #5fff46">Umikawaii</a></span>
        <span style="color: #fff; font-weight: 700; margin-right: 20px;">© UmiBlog 2022 - Website đọc Truyện
            Tranh</span>
    </div>
</footer>
<!-- <script>
      let header = document.querySelector('header');
      let headroom = new Headroom(header);
      headroom.init();
    </script> -->
<script>
$(document).ready(function() {
    $('#sidebarCollapse').on('click', function() {
        $('#sidebar').toggleClass('active');
    });
});
</script>


<script>
jQuery(function($) {
    var path = window.location.href;

    $('ul a').each(function() {
        if (this.href === path) {
            $(this).addClass('active');
        }
    });
});
</script>

@endsection