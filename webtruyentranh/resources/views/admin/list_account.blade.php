@extends('layouts.admin')

@section('title', 'Admin')

@section('body')

<!--content-->
<section>
    <div class="content1">
        <div class="data-content">
            <form class="d-flex">

            </form>
            <div class="search-manager">
                <form class="d-flex" autocomplete="off" class="d-flex" method="GET" accept-charset="UTF-8"
                    action="{{ route('search-email') }}">
                    Lọc theo:
                    <a href="{{route('list-account')}}" data-bs-toggle="tooltip" title="Lọc tất cả tài khoản">
                        <button type="button" class="btn btn-primary">
                            <i class="fa fa-filter"></i>
                        </button>
                    </a>

                    <a href="{{route('list-account-enable')}}" data-bs-toggle="tooltip"
                        title="Lọc tài khoản đang hoạt động">
                        <button type="button" class="btn btn-success">
                            <i class="fa fa-check-square"></i>
                        </button>
                    </a>

                    <a href="{{route('list-account-disable')}}" data-bs-toggle="tooltip"
                        title="Lọc tài khoản ngưng hoạt động">
                        <button type="button" class="btn btn-danger">
                            <i class="fa fa-close"></i>
                        </button>
                    </a>

                    <input id="keywords" name="keywords" class="form-control me-4" type="text" placeholder="Tìm email">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <form>
                <div class="table-content">
                    <p class="title-table">
                        <div class="title-main">
                            <span class="title-1"><i class="fa fa-hourglass-start"></i>Tài</span>
                            <span class="title-2">Khoản</span>
                            <!-- <span class="button-create">
                                        <button class="btn btn-success">Thêm Truyện</button>
                                    </span> -->
                        </div>
        
                        </p>
                    <table class="table table-striped secondary table-bordered">
                        <thead>
                            <tr>
                                <th>Avatar</th>
                                <th>Email</th>
                                <th>Họ Tên</th>
                                <th>Trạng Thái</th>
                                <th>Chức Năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($listAccount as $value)
                            <tr>
                                <td><img class="rounded-circle" src="{{$value->avatar}}" width="80" height="80"
                                        loading="lazy"></td>
                                <td>{{$value->email}}</td>
                                <td>{{$value->name}}</td>
                                @if ($value->status_id==1)
                                <td class="status-enable">Hoạt Động </td>
                                @endif

                                @if ($value->status_id==2)
                                <td class="status-disable">Ngưng hoạt Động </td>
                                @endif

                                <td>
                                    @if ($value->status_id == 1)
                                    <a href="{{route('change-status-account', ['id' => $value->id])}}"
                                        data-bs-toggle="tooltip" title="Khóa tài khoản">
                                        <button type="button" class="btn btn-danger">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </a>
                                    @endif

                                    @if ($value->status_id == 2)
                                    <a href="{{route('change-status-account', ['id' => $value->id])}}"
                                        data-bs-toggle="tooltip" title="Kích hoạt tài khoản">
                                        <button type="button" class="btn btn-success">
                                            <i class="fa fa-check-square"></i>
                                        </button>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </form>
        </div>
</section>
<div class="text-center center-pagination">
    {{ $listAccount->links() }}
</div>

@endsection