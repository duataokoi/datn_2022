@extends('layouts.admin')

@section('title', 'Admin')

@section('body')
<!-- danh chon -->
<section>
    <div class="content1">
        <div class="data-content">
            <div class="search-manager">
                <form autocomplete="off" class="d-flex" method="GET" accept-charset="UTF-8"
                    action="{{ route('search-story-admin') }}">
                    <input id="keywords" name="keywords" class="form-control me-2" type="text" placeholder="Tìm Truyện">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <div class="table-content">
                <p class="title-table">
                <div class="title-main">
                    <span class="title-1"><i class="fa fa-hourglass-start"></i>Danh Sách</span>
                    <span class="title-2">Báo Cáo</span>
                </div>

                </p>
                <table class="table table-striped secondary table-bordered">
                    <thead>
                        <tr>
                            <th>Bìa Truyện</th>
                            <th>Tên Truyện</th>
                            <th>Nội Dung Báo Cáo</th>
                            <th>Người Báo Cáo</th>
                            <th>Chức Năng</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($report as $value)
                        <tr>
                            <td><img src="{{$value->story->image}}" height="150" loading="lazy"></td>
                            <td>
                                <a
                                    href="{{ route('get-detail-story',['id'=>$value->story->id]) }}">{{ $value->story->name }}</a>
                            </td>
                            <td>{{ $value->content }}</td>
                            <td>
                                {{ $value->account->name }}
                            </td>

                            <td>
                                <a href="{{ route('delete-report',['id'=>$value->id]) }}">
                                    <button type="button" class="btn btn-success" data-bs-toggle="tooltip"
                                        title="Giữ truyện">
                                        <i class="fa fa-check-square-o"></i>
                                    </button>
                                </a>

                                <a href="{{ route('delete-story-report',['id'=>$value->story->id]) }}">
                                    <button type="button" class="btn btn-danger" data-bs-toggle="tooltip"
                                        title="Ẩn truyện">
                                        <i class="fa fa-close"></i>
                                    </button>
                                </a>

                            </td>
                        </tr>
                        @empty
                        <span>Không có báo cáo</span>
                        @endforelse

                    </tbody>
                </table>
            </div>
        </div>
</section>
</div>
<div class="text-center center-pagination">
    {{ $report->appends(request()->all())->links() }}
</div>
<!-- <div class="spinner-border text-primary" role="status">
        <span class="sr-only">Loading...</span>
      </div> -->
<!-- bootstrap -->
<footer style="background-color: #333; padding: 30px 10px;">
    <div class="container">
        <span class="right" style="color: #fff; font-weight: 700; margin-right: 20px; float: right;">Liên hệ: <a
                href="mailto:0306191423@caothang.edu.vn" target="_blank" style="color: #5fff46">Umikawaii</a></span>
        <span style="color: #fff; font-weight: 700; margin-right: 20px;">© UmiBlog 2022 - Website đọc Truyện
            Tranh</span>
    </div>
</footer>
<!-- <script>
      let header = document.querySelector('header');
      let headroom = new Headroom(header);
      headroom.init();
    </script> -->
<script>
$(document).ready(function() {
    $('#sidebarCollapse').on('click', function() {
        $('#sidebar').toggleClass('active');
    });
});
</script>


<script>
jQuery(function($) {
    var path = window.location.href;

    $('ul a').each(function() {
        if (this.href === path) {
            $(this).addClass('active');
        }
    });
});
</script>

@endsection