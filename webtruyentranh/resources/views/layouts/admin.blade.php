<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://unpkg.com/headroom.js"></script>
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/login.css') }}" rel="stylesheet">
    <link rel="icon" href="{{ asset('images/logo.png') }}" type="image/x-icon">
    <title>@yield('title')</title>
</head>

<body>
    <div class="vc">
        <nav class="navbar navbar-expand-sm  bg-dark ">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ route('list-story-admin')}}">Bảng Điều Khiển</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="mynavbar">
                    <ul class="navbar-nav me-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('home')}}"><i class="fa fa-fw fa-home"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('list-account')}}"><i class="fa fa-fw fa-edit"></i>Quản Lí
                                Tài Khoản</a>
                        </li>
                        {{-- <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)">Quản Lí Thể Loại</a>
                  </li> --}}
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"><i
                                    class="fa fa-fw fa-edit"></i>Quản Lí Thể Loại</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('list-category') }}">Danh Sách Thể Loại</a>
                                </li>
                                <li><a class="dropdown-item" href="{{ route('show-create-category') }}">Thêm Thể
                                        Loại</a></li>

                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"><i
                                    class="fa fa-fw fa-edit"></i>Quản Lí Truyện</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('list-story-admin') }}">Truyện Đã Đăng</a>
                                </li>
                                <li><a class="dropdown-item" href="{{ route('list-story-disable-admin') }}">Truyện Đã
                                        Ẩn</a></li>

                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('list-report') }}"><i class="fa fa-fw fa-edit"></i>Quản
                                Lí
                                Báo Cáo</a>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="">
                <div class=" nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#"><img
                            src="{{ auth()->user()->avatar }}" class="rounded-pill" width="50" height="50"
                            loading="lazy"></a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{route('user-info',['id' => auth()->user()->id])}}"><i
                                    class="fa fa-user"></i>Trang Cá Nhân</a></li>
                        @if(auth()->user()->type_account_id==2)
                        <li><a class="dropdown-item" href="{{ route('list-story-user') }}"><i class="fa fa-pencil"></i>
                                Quản Lí </a></li>
                        @else
                        <li><a class="dropdown-item" href="{{ route('list-story-admin') }}"><i class="fa fa-pencil"></i>
                                Quản Lí </a></li>
                        @endif
                        @if(auth()->user()->type_account_id==2)
                        <li><a class="dropdown-item" href="#"><i class="fa fa-heart"></i> Truyện Yêu Thích</a></li>
                        @endif
                        <li><a class="dropdown-item" href="{{ route('log-out') }}"><i class="fa fa-power-off"></i> Đăng
                                Xuất</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        @yield('body')