<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--bootstrap-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!--end bootstrap-->
    <link rel="icon" href="{{ asset('images/logo.png') }}" type="image/x-icon">
    <!--owl.carousel-->
    <link rel="stylesheet" href="./owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="./owlcarousel/assets/owl.theme.default.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="owlcarousel/owl.carousel.min.js"></script>


    <!--chosen-->
    <link href="{{ asset('/chosen/chosen.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('/chosen/chosen.jquery.min.js')}}" type="text/javascript"></script>
    <!--end chosen-->
    <!--ckediter-->
    <script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
    <!--end ckediter-->

    <!--end owl.carousel-->
    <link rel="stylesheet" href="./css/@yield('css')">
    <link href="{{ asset('/css/edit_story.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">

    <script src="{{ asset('/js/js.js') }}"></script>
    <title>@yield('title')</title>
</head>

<body>
    <div class="">
        <nav class="navbar navbar-expand-sm navbar bg-dark ">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ route('list-story-user')}}">Bảng Điều Khiển</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="mynavbar">
                    <ul class="navbar-nav me-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('home')}}"><i class="fa fa-fw fa-home"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('show-create-story') }}"><i class="fa fa-fw fa-pencil-square"></i>Thêm Truyện</a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">Quản Lí
                                Truyện</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('list-story-user') }}">Truyện Đã Đăng</a>
                                </li>
                                <li><a class="dropdown-item" href="{{ route('list-story-disable-user') }}">Truyện Đã
                                        Ẩn</a></li>

                            </ul>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="">
                <div class=" nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#"><img src="{{ auth()->user()->avatar }}" class="rounded-pill" height="50" width="50"></a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{route('user-info',['id' => auth()->user()->id])}}"><i class="fa fa-user"></i> Trang Cá Nhân</a></li>
                        @if(auth()->user()->type_account_id==2)
                        <li><a class="dropdown-item" href="{{ route('list-story-user') }}"><i class="fa fa-pencil"></i>
                                Quản Lí </a></li>
                        @else
                        <li><a class="dropdown-item" href="{{ route('list-story-admin') }}"><i class="fa fa-pencil"></i>
                                Quản Lí </a></li>
                        @endif
                        @if(auth()->user()->type_account_id==2)
                        <li><a class="dropdown-item" href="{{ route('get-list-story-favourite') }}"><i class="fa fa-heart"></i> Truyện Yêu Thích</a></li>
                        @endif
                        <li><a class="dropdown-item" href="{{ route('log-out') }}"><i class="fa fa-power-off"></i> Đăng
                                Xuất</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        @yield('body')

</body>

</html>