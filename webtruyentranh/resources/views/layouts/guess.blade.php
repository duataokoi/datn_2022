<!DOCTYPE html>
<html lang="en">

<body>

    <nav class="navbar navbar-expand-sm  bg-dark ">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ route('home') }}"><img src=" {{ asset('/images/logo.png') }}" height="50"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="mynavbar">
                <ul class="navbar-nav me-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="" role="button" data-bs-toggle="dropdown">Thể
                            Loại</a>
                        <ul class="dropdown-menu">
                            @foreach($listCategory as $value)
                            <li><a class="dropdown-item" href="{{ route('get-list-story-catelogy',['slug'=>$value->name_convert]) }}">{{$value->name}}</a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </div>
            <form autocomplete="off" class="d-flex" method="GET" accept-charset="UTF-8" action="{{ route('search') }}">
                @csrf
                <input id="keywords" name="keywords" class="form-control me-2" type="text" placeholder="Tìm Kiếm">
                <button class="btn btn-primary" disabled="true" id="submit" type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div>
        <div class="">
            @if(empty(auth()->user()))
            <div class="button-login-register">
                <div>
                    <span><a class="login-register" href="{{ route('log-in') }}">Đăng Nhập</a></span>

                    <span class="space-button-login-register"></span>
                    <span><a class="login-register" href="{{ route('sign-up') }}">Đăng Kí</a></span>
                </div>
            </div>
            @else
            <div class=" nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href=""><img src="{{ auth()->user()->avatar }}" class="rounded-pill" height="50" width="50"></a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="{{route('user-info',['id' => auth()->user()->id])}}"><i class="fa fa-user"></i> Trang Cá Nhân</a></li>
                    @if(auth()->user()->type_account_id==2)
                    <li><a class="dropdown-item" href="{{ route('list-story-user') }}"><i class="fa fa-pencil"></i>
                            Quản Lí </a></li>
                    @else
                    <li><a class="dropdown-item" href="{{ route('list-story-admin') }}"><i class="fa fa-pencil"></i>
                            Quản Lí </a></li>
                    @endif
                    @if(auth()->user()->type_account_id==2)
                    <li><a class="dropdown-item" href="{{ route('get-list-story-favourite') }}"><i class="fa fa-heart"></i> Truyện Yêu Thích</a></li>
                    @endif
                    <li><a class="dropdown-item" href="{{ route('log-out') }}"><i class="fa fa-power-off"></i> Đăng
                            Xuất</a></li>
                </ul>
            </div>
            @endif
        </div>
    </nav>


    @yield('body')

    <footer style="background-color: #333; padding: 30px 10px;">
        <div class="container">
            <span class="right" style="color: #fff; font-weight: 700; margin-right: 20px; float: right;">Liên hệ: <a href="mailto:0306191423@caothang.edu.vn" target="_blank" style="color: #5fff46">Umikawaii</a></span>
            <span style="color: #fff; font-weight: 700; margin-right: 20px;">© HLManga 2022 - Website đọc Truyện
                Tranh</span>
        </div>

    </footer>
    <!-- owl-carousel-slide-show -->
    <script>
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            lazyLoad: true,
            autoplay: true,
            autoplayHoverPause: true,
            autoplayTimeout: 3000,
            smartSpeed: 500,
            slidesToShow: 4,
            navText: ['<i class="fa fa-arrow-circle-left" nav-icon></i>',
                '<i class="	fa fa-arrow-circle-right" nav-icon></i>'
            ],
            // mouseDrag:true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 4
                }
            }
        })
    </script>
    <script>
        $('#keywords').on('keyup', function() {
            $value = $('#keywords').val().trim()
            if ($value.length > 0) {
                document.getElementById("submit").disabled = false;
            } else {
                document.getElementById("submit").disabled = true;
            }

        });
    </script>
    <!-- end-owl-carousel-slide-show -->
</body>

</html>