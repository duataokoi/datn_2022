<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://unpkg.com/headroom.js"></script>
    <link rel="stylesheet" href="./css/login_register.css">
    <link rel="stylesheet" href="./css/style.css">
    <link rel="icon" href="{{ asset('images/logo.png') }}" type="image/x-icon">
    <title>Register</title>
</head>

<body>
    <div class="vc">
        <nav class="navbar navbar-expand-sm navbar bg-dark ">
            <div class="container-fluid">
                <a class="navbar-brand" href="javascript:void(0)">Bảng Điều Khiển</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="mynavbar">

                </div>

            </div>
            <div class="button-login-register">
                <div>
                    <span><a class="login-register" href="{{ route('log-in') }}">Đăng Nhập</a></span>

                    <span class="space-button-login-register"></span>
                    <span><a class="login-register" href="">Đăng Kí</a></span>
                </div>
            </div>
        </nav>
    </div>


    <main class="container">
        <div class="text-center">
            <div class="form-register">
                <h4 class="title-form">
                    Đăng Kí
                </h4>
                <form method="POST" action="{{ route('sign-up') }}">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Họ Tên</label>
                        <input type="text" class="form-control" name="name" id="exampleInputEmail1"
                            aria-describedby="emailHelp" placeholder="Họ Tên">
                        @error('name')
                        <span class='text-danger' role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Địa Chỉ Email</label>
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1"
                            aria-describedby="emailHelp" placeholder="Email">
                        @error('email')
                        <span class='text-danger' role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Mật Khẩu</label>
                        <input type="password" name="password" class="form-control" id="password"
                            placeholder="Mật Khẩu">
                        @error('password')
                        <span class='text-danger' role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Xác Nhận Mật Khẩu</label>
                        <input type="password" name="re_password" class="form-control" id="re_password"
                            placeholder="Xác Nhận Mật Khẩu">
                        @error('re_password')
                        <span class='text-danger' role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <span id='message'></span>

                    </div>

                    <!-- <div class="form-group form-check">

                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Tôi đồng ý với các điều khoản</label>
                    </div> -->

                    <button type="submit" id="submit" disabled="disabled" class="btn btn-primary">Đăng Kí</button>
                </form>
            </div>
        </div>

    </main>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script>
    $('#password, #re_password').on('keyup', function() {
        if (($('#password').val() == $('#re_password').val()) && ($('#password').val().length >= 8)) {
            $('#message').html('Invalid').css('color', 'green');
            document.getElementById("submit").disabled = false;
        } else {
            $('#message').html('UnInvalid').css('color', 'red');
            document.getElementById("submit").disabled = true;
        }

    });
    </script>
</body>

</html>