<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://unpkg.com/headroom.js"></script>

    <link rel="stylesheet" href="{{ asset('css/read_chapter.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="icon" href="{{ asset('images/logo.png') }}" type="image/x-icon">
    <title>Read-chapter</title>
</head>

<!--nut thanh ben phai duoi cung-->
<section id="rd_side_icon">
    @if($chapter->id == $min_id->id)
    <!--kiem tra chap dau-->
    <a href="" class="rd_sd-button_item col text-center {{ $chapter->id == $min_id->id ? 'isDisable' : '' }}"><i
            class="fa fa-backward"></i></a>
    @else
    <a href="{{ route('read-chapter',['id'=>$previous_chapter_id]) }}" class="rd_sd-button_item col text-center "><i
            class="fa fa-backward"></i></a>
    @endif

    <a href="{{ route('get-detail-story',['id'=>$chapter->story_id]) }}" class="rd_sd-button_item col text-center"><i
            class="fa fa-home"></i></a>

    @if($chapter->id == $max_id->id)
    <!--kiem tra chap cuoi-->
    <a href="" class="rd_sd-button_item col text-center {{ $chapter->id == $max_id->id ? 'isDisable' : '' }}"><i
            class="fa fa-forward"></i></a>
    @else
    <a href="{{ route('read-chapter',['id'=>$next_chapter_id]) }}"
        class="rd_sd-button_item col text-center {{ $chapter->id == $max_id->id ? 'isDisable' : '' }}"><i
            class="fa fa-forward"></i></a>
    @endif
</section>
<!--end nut thanh ben phai duoi cung-->


<header>
    <div class="title-story col-md-12">
        {{ $story->story->name }}
    </div>
    <div class="title-chapter">
        {{ $chapter->name }}
    </div>
</header>


<div class="d-flex justify-content-center">
    <div class="content">
        <section class="rd-basic_icon toolbar">
            @if($chapter->id == $min_id->id)
            <a href="" class="col icon-backwrad text-center {{ $chapter->id == $min_id->id ? 'isDisable' : '' }}"><i
                    class="fa fa-backward"></i></a>
            @else
            <a href="{{ route('read-chapter',['id'=>$previous_chapter_id]) }}" class="col icon-backwrad text-center "><i
                    class="fa fa-backward"></i></a>
            @endif

            <select name="select-chapter" id="ac" class="custom-select select-chapter">
                @foreach ( $all_chapter as $chap )
                <option value="{{ route('read-chapter',['id'=>$chap->id]) }}">{{ $chap->name }}</option>
                @endforeach
            </select>

            @if($chapter->id == $max_id->id)
            <!--kiem tra chap cuoi-->
            <a href="" class="rd_sd-button_item col text-center {{ $chapter->id == $max_id->id ? 'isDisable' : '' }}"><i
                    class="fa fa-forward"></i></a>
            @else
            <a href="{{ route('read-chapter',['id'=>$next_chapter_id]) }}"
                class="rd_sd-button_item col text-center {{ $chapter->id == $max_id->id ? 'isDisable' : '' }}"><i
                    class="fa fa-forward"></i></a>
            @endif

        </section>

        <section class="content-chapter">
            @foreach ( $chapter_image as $image)
            <img class="img-fluid" src="{{ $image->name }}" alt="" width=" 807" height=" 1149 " loading="lazy">
            @endforeach
        </section>
        <div class="button-home-story d-grid">
            <a href="{{ route('get-detail-story',['id'=>$chapter->story_id]) }}"
                class="btn btn-primary btn-lg btn-block" role="button">QUAY VỀ TRANG TRUYỆN</a>
        </div>

        <section class="rd-basic_icon toolbar">
            @if($chapter->id == $min_id->id)
            <a href="" class="col icon-backwrad text-center {{ $chapter->id == $min_id->id ? 'isDisable' : '' }}"><i
                    class="fa fa-backward"></i></a>
            @else
            <a href="{{ route('read-chapter',['id'=>$previous_chapter_id]) }}" class="col icon-backwrad text-center "><i
                    class="fa fa-backward"></i></a>
            @endif

            <select name="select-chapter" id="abc" class="custom-select select-chapter">
                @foreach ( $all_chapter as $chap )
                <option value="{{ route('read-chapter',['id'=>$chap->id]) }}">{{ $chap->name }}</option>
                @endforeach
            </select>

            @if($chapter->id == $max_id->id)
            <!--kiem tra chap cuoi-->
            <a href="" class="rd_sd-button_item col text-center {{ $chapter->id == $max_id->id ? 'isDisable' : '' }}"><i
                    class="fa fa-forward"></i></a>
            @else
            <a href="{{ route('read-chapter',['id'=>$next_chapter_id]) }}"
                class="rd_sd-button_item col text-center {{ $chapter->id == $max_id->id ? 'isDisable' : '' }}"><i
                    class="fa fa-forward"></i></a>
            @endif
        </section>

    </div>
</div>
<footer style="background-color: #333; padding: 30px 10px;">
    <div class="container">
        <span class="right" style="color: #fff; font-weight: 700; margin-right: 20px; float: right;">Liên hệ: <a
                href="mailto:0306191423@caothang.edu.vn" target="_blank" style="color: #5fff46">Umikawaii</a></span>
        <span style="color: #fff; font-weight: 700; margin-right: 20px;">© UmiBlog 2022 - Website đọc Truyện
            Tranh</span>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script type="text/javascript">
$('.select-chapter').on('change', function() {
    var url = $(this).val();
    if (url) {
        window.location = url;
    }
    return false;
});

current_chapter();

function current_chapter() {
    var url = window.location.href;
    $('.select-chapter').find('option[value="' + url + '"]').attr("selected", true);
}
</script>
</body>

</html>