<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class account extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'email' => 'required',
            'password' => 'required|min:8',
            're_password' => 'required|min:8',
            'name' => 'required|min:3'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Hãy nhập email',
            'password.min' => 'Email phải có ít nhất :min kí tự',
            'password.required' => 'Hãy nhập mật khẩu',
            'password.min' => 'Mật khẩu phải có ít nhất :min kí tự',
            're_password.required' => 'Hãy nhập xác nhận mật khẩu',
            're_password.min' => 'Xác nhận mật khẩu phải có ít nhất :min kí tự',
            'name.required' => 'Hãy nhập tên của bạn',
            'name.min' => 'Tên của bạn phải có ít nhất :min kí tự',
            
        ];        
    }
}
