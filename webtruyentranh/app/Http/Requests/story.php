<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class story extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'name_convert' => 'required',
            'other_name' => 'required',
            'author' => 'required',
            'author_convert' => 'required',
            'category' => 'required',
            'image' => 'required',
            'description' => 'required|min:20'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Hãy nhập tên truyện',
            'name_convert.required' => 'Vui lòng nhập lại tên truyện',
            'other_name.required' => 'Hãy nhập tên khác',
            'author.required' => 'Hãy nhập tên tác giả',
            'author_convert.required' => 'Vui lòng nhập lại tên tác giả',
            'category.required' => 'Hãy chọn thể loại truyện',
            'image.required' => 'Hãy thêm ảnh',
            'description.required' => 'Hãy nhập mô tả',
            'description.min' => 'Nhập mô tả ít nhất :min kí tự'
        ];
    }
}