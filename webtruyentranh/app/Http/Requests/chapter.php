<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class chapter extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
   public function rules()
    {
        return [
            'name' => 'required',
            'image' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Hãy nhập tên chương',
            'image.required' => 'Hãy thêm ảnh'
        ];        
    }
}
