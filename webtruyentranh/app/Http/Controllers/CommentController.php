<?php

namespace App\Http\Controllers;
use App\Models\comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    public function commentStory(Request $request)
    {
       
        $comment = new comment;
        $comment->story_id = $request->story_id;
        $comment->account_id = $request->account_id;
        $comment->content = $request->content;
        $comment->save();
        return 0;
    }

    public function saveComment(Request $request)
    {
        $comment=comment::find($request->id);
        $comment->content=$request->content;
        $comment->save();
        return 0;
    }

    public function deleteCommentStory(Request $request)
    {
       $comment=comment::find($request->id);
       $comment->delete();
       return 0;
    }
}
