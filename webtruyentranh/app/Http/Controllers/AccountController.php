<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests\account as AccountRequest;
use App\Http\Requests\changeInfo as changeInfoRequest;

use App\Models\account;
use App\Models\report;
use App\Models\story;
use App\Models\favouritestory;
use App\Models\comment;
use App\Models\chapter;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
// use App\Http\Controllers\Redirect;
// use Illuminate\Support\Facades\Redirect;
use Redirect;
use Cloudder;

class AccountController extends Controller
{
    public function viewLogin()
    {
        redirect()->setIntendedUrl(url()->previous());
        return view('log_in');
    }

    public function logIn(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' =>
        $request->password])) {
            return redirect()->intended('home');
        } else {
            return back()->with('error', 'Email hoặc mật khẩu sai, vui lòng thử lại');
        }
    }

    public function home()
    {
        $outstanding = story::where('status_id', 1)->orderBy('view', 'desc')->take(8)->get();
        
        
        $chapter = chapter::select(['chapter.*'])->with(['story' => function ($query)  {
            $query->where([['status_id', 1], ['state', 2]])->orderBy('updated_at', 'desc');
        }])->
        
        whereHas('story', function ($query1)  {
            $query1->where([['status_id', 1], ['state', 2]])->orderBy('updated_at', 'desc');
        })->orderBy('created_at', 'desc')->get();
        $complete = $chapter->unique('story_id')->toQuery()->orderBy('updated_at', 'DESC')->take(3)->get();
        


        $new_create = story::where('status_id', 1)->orderBy('created_at', 'desc')->take(6)->get();


       
        $a = chapter::with('story')->orderBy('created_at', 'DESC')->get();
        $new_chapter = $a->unique('story_id')->take(7);
        // $story_detail=$new_chapter->story->first();

        $top_favourite = favouritestory::with('story')->select('story_id', favouritestory::raw('count(*) as favourites'))->groupBy('story_id')->orderBy('favourites', 'DESC')->take(10)->get();

        $list_story_random=story::where('status_id',1)->inRandomOrder()->take(4)->get();

        return view('home', compact('outstanding',  'complete', 'new_create', 'new_chapter', 'top_favourite','list_story_random'));
    }

    public function viewSignup()
    {

        if (empty(auth()->user())) {
            redirect()->setIntendedUrl(url()->previous());
            return view('register');
        }
        return redirect()->route('home');
    }

    public function userInfo($id)
    {
        $user=account::find($id);
       
        return view('user/user_info',compact('user'));
    }

    public function listChapter($id)
    {
        $story = story::find($id);
        $chapter = chapter::with('story')->orderBy('id', 'DESC')->where('story_id', $id)->withTrashed()->get();

        return view('user/list_chapter',  compact('chapter', 'story'));
    }

    public function deleteChapter($id)
    {
        $chapter = chapter::find($id);
        $chapter->delete();
        $story = story::find($chapter->story_id);

        return redirect()->route('list-chapter', ['id' => $story->id]);
    }

    public function restoreChapter($id)
    {
        $chapter = chapter::withTrashed()->find($id);
        $chapter->restore();
        $story = story::find($chapter->story_id);

        return redirect()->route('list-chapter', ['id' => $story->id]);
    }

    public function listStoryByUser()
    {
        $listStory = story::with('ListStoryCategory')->where('account_id', auth()->user()->id)->where('status_id', 1)->orderBy('created_at', 'DESC')->paginate(5);

        return view('user/user', compact('listStory'));
    }

    public function listStoryDisableByUser()
    {
        // $listStory = story::with('ListStoryCategory')->where('account_id',auth()->user()->id)->where('status_id',2)->orderBy('id','DESC')->get();
        $listStory = story::onlyTrashed()->where('account_id', auth()->user()->id)->orderBy('id', 'DESC')->paginate(5);
        return view('user/list_story_disable_user', compact('listStory'));
    }

    public function signUp(AccountRequest $request)
    {
        $checkEmail = account::where('email', $request->email);
        if (!empty($checkEmail)) {
            $account = new account;
            $account->name = $request->name;

            $account->type_account_id = 2; //user
            $account->password = Hash::make($request->password);
            $account->email = $request->email;
            $account->avatar = 'https://res.cloudinary.com/hlmanga/image/upload/v1657246248/avatar_default/avatar_lkkepx_di9pst.jpg';
            $account->status_id = 1;
            $account->save();
            if (Auth::attempt(['email' => $request->email, 'password' =>
            $request->password])) {

                return redirect()->intended('home');
            } else {
                echo ("fail");
            }
        }
    }

    public function logOut()
    {
        $user = Auth::logout();
        return redirect()->route('home');
    }

    public function getListStoryFavourite()
    {

        $listStory = favouritestory::with('story')->where('account_id', auth()->user()->id)->paginate(10);
        return view('user/favourite_story', compact('listStory'));
    }

    public function deleteFavouriteStory($id)
    {
        $favourite = favouritestory::where('id', $id)->delete();
        return redirect()->back()->with('status', 'Xóa yêu thích thành công!');
    }

    


    public function listAccount()
    {
        $listAccount = account::paginate(5);
        return view('admin/list_account', compact('listAccount'));
    }

    public function changeStatusAccount($id)
    {
        $account = account::find($id);
        if ($account->status_id == 1) {
            $account->status_id = 2;
            $account->save();
            return redirect()->route('list-account');
        }

        if ($account->status_id == 2) {
            $account->status_id = 1;
            $account->save();
            return redirect()->route('list-account');
        }
    }

    public function listAccountEnable()
    {
        $listAccount = account::where('status_id', 1)->orderBy('id', 'DESC')->paginate(5);
        return view('admin/list_account', compact('listAccount'));
    }

    public function listAccountDisable()
    {
        $listAccount = account::where('status_id', 2)->orderBy('id', 'DESC')->paginate(5);
        return view('admin/list_account', compact('listAccount'));
    }

    //admin
    public function listStoryByAdmin()
    {
        $listStory = story::with('ListStoryCategory')->where('status_id', 1)->orderBy('id', 'DESC')->paginate(5);

        return view('admin/admin', compact('listStory'));
    }
    public function listStoryDisableByAdmin()
    {
        // $listStory = story::with('ListStoryCategory')->where('status_id',2)->orderBy('id','DESC')->get();
        $listStory = story::onlyTrashed()->orderBy('id', 'DESC')->paginate(5);
        return view('admin/list_story_disable', compact('listStory'));
    }

    public function changeUserInfo($id, changeInfoRequest $request)
    {
        $account = account::find($id);
        $account->name = $request->name;
        if (!empty($request->avatar)) {
            $image_name = $request->file('avatar');
            $image_name->getRealPath();
            Cloudder::upload($image_name, null, array(
                "folder" => "avatar/$account->email",  "overwrite" => FALSE,
                "resource_type" => "image", "responsive" => TRUE, "transformation" => array("quality" => "70", "width" => "1000", "height" => "1423", "crop" => "scale")
            ));
            $image_url = Cloudder::getResult();
            $account->avatar = $image_url['secure_url'];
        }
        if (!empty($request->password)) {
            $account->password = Hash::make($request->password);
        }
        $account->save();
        return redirect()->back();
    }

    public function searchEmail()
    {
        $keywords = $_GET['keywords'];
        $name = $_GET['keywords'];
        $listAccount = account::where("status_id", 1)->where('email', 'LIKE', '%' . $keywords . '%')->orWhere('name', 'LIKE', '%' . $keywords . '%')->where("status_id", 1)->paginate(5);

        return view('admin/list_account')->with(compact('name', 'listAccount'));
    }

    public function searchStoryAdmin()
    {
        $keywords = $_GET['keywords'];
        $name = $_GET['keywords'];
        $listStory = story::where("status_id", 1)->where('name', 'LIKE', '%' . $keywords . '%')->orWhere('author', 'LIKE', '%' . $keywords . '%')->where("status_id", 1)->paginate(5);
        return view('admin/admin')->with(compact('name', 'listStory'));
    }
    public function searchStoryUserLogin($id)
    {
        $keywords = $_GET['keywords'];
        $name = $_GET['keywords'];
        $listStory = story::where("status_id", 1)->where('account_id', $id)->where('name', 'LIKE', '%' . $keywords . '%')->orWhere('author', 'LIKE', '%' . $keywords . '%')->where('account_id', $id)->where("status_id", 1)->paginate(5);
        return view('user/user')->with(compact('name', 'listStory'));
    }
    public function searchStoryDisable()
    {
        $keywords = $_GET['keywords'];
        $name = $_GET['keywords'];
        if (auth()->user()->type_account_id == 1) {
            $listStory = story::onlyTrashed()->where("status_id", 2)->where('name', 'LIKE', '%' . $keywords . '%')->orWhere('author', 'LIKE', '%' . $keywords . '%')->where("status_id", 2)->paginate(5);
            return view('admin/list_story_disable')->with(compact('name', 'listStory'));
        }
        $listStory = story::onlyTrashed()->where("status_id", 2)->where('account_id', auth()->user()->id)->where('name', 'LIKE', '%' . $keywords . '%')->orWhere('author', 'LIKE', '%' . $keywords . '%')->where("status_id", 2)->where('account_id', auth()->user()->id)->paginate(5);
        return view('user/list_story_disable_user')->with(compact('name', 'listStory'));
    }

    public function listReport()
    {
        $report = report::orderBy('id', 'DESC')->paginate(5);
        return view('admin/report', compact('report'));
    }
    public function deleteReport($id)
    {
        report::find($id)->delete();
        return redirect()->route('list-report');
    }
    public function deteleStoryReport($id)
    {
        $Story = story::find($id);
        $Story->status_id = 2;
        $Story->save();
        $Story->delete();
        report::where('story_id', $Story->id)->delete();

        favouritestory::where('story_id', $Story->id)->delete();

        chapter::with('story')->where('story_id', $id)->delete();
        return redirect()->route('list-report');
    }
   
}
