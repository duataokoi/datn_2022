<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\category;
use App\Http\Requests\categoryofstory as CategoryRequest;


class CategoryController extends Controller
{
    public function addNewCategory(CategoryRequest $request)
    {
        $category = new category;
        $category->name = $request->name;
        $category->name_convert = $request->name_convert;
        $category->save();
        return view('admin/create_category');
    }
    public function showCreateCategory()
    {
        return view('admin/create_category');
    }
    public function listCategory()
    {
        $category = category::orderBy('name', 'ASC')->paginate(10);
        return view('admin/list_category', compact('category'));
    }
    public function showEditCategory($id)
    {
        $category = category::find($id);
        return view('admin/edit_category', compact('category'));
    }
    public function editCategory($id, CategoryRequest  $request)
    {
        $category = category::find($id);
        $category->name = $request->name;
        $category->name_convert = $request->name_convert;
        $category->save();
        return redirect()->route('list-category');
    }
}