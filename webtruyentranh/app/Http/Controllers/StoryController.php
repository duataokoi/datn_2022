<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\story;
use App\Models\chapter;
use App\Models\category;
use App\Models\report;
use App\Models\account;
use App\Models\imagechapter;
use App\Models\favouritestory;
use App\Models\rate;
use App\Models\comment;
use App\Models\category_of_story;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\chapter as ChapterRequest;
use App\Http\Requests\story as StoryRequest;
use App\Http\Requests\edit_story as EditStoryRequest;
use App\Http\Requests\edit_chapter as EditChapterRequest;


use Cloudder;

class StoryController extends Controller
{
    public function getListStory()
    {
        $storyList = story::where('status_id', 1)->get();
        return view('list_story');
    }

    public function getDetailStory($id)
    {

        $detail_story = story::with('ListStoryCategory')->where('id', $id)->where('status_id', 1)->first();

        $user_upload = account::find($detail_story->account_id);

        $chapter = chapter::with('story')->orderBy('id', 'DESC')->where('story_id', $id)->get();

        $list_story_user = story::where('account_id', $detail_story->account_id)->inRandomOrder()->take(4)->get();

        $list_story_outstanding = story::where('status_id', 1)->orderBy('view', 'desc')->take(4)->get();


        $detail_story->view = $detail_story->view + 1;
        $detail_story->save();



        if (Auth::check()) {
            $ratingUser = rate::where('story_id', $detail_story->id)->where('account_id', Auth()->user()->id)->first();
            $favouritedUser = favouritestory::where('story_id', $detail_story->id)->where('account_id', Auth()->user()->id)->first();
        } else {
            $ratingUser = 0;
            $favouritedUser = 0;
        }
        $rating = rate::where('story_id', $detail_story->id)->get();
        $ratingAvg = rate::where('story_id', $detail_story->id)->avg('number_star');
        $favourite = favouritestory::where('story_id', $detail_story->id)->get();

        $comment = comment::with('story')->where('story_id', $id)->orderBy('created_at', 'DESC')->get();


        return view('detail_story', compact('detail_story', 'chapter', 'list_story_user', 'list_story_outstanding', 'user_upload', 'favourite', 'favouritedUser', 'ratingUser', 'ratingAvg', 'rating', 'comment'));
    }

    public function readChapter($id)
    {

        $chapter_image = imagechapter::with('chapter')->orderBy('numberical_order', 'ASC')->where('chapter_id', $id)->get();
        // dd($chapter_image);
        $chapter = chapter::where('id', $id)->first();
        $all_chapter = chapter::with('story')->where('story_id', $chapter->story_id)->orderBy('id', 'DESC')->get();
        $story = chapter::with('story')->where('story_id', $chapter->story_id)->first();


        // $next_chapter = Chapter::where('novel_id', $novel->novel_id)->where('id', '>', $chapter->id)->min('slug_chapter');
        $next_chapter_id = Chapter::where('story_id', $story->story_id)->where('id', '>', $chapter->id)->min('id');

        // $previous_chapter = Chapter::where('novel_id', $novel->novel_id)->where('id', '<', $chapter->id)->max('slug_chapter');
        $previous_chapter_id = Chapter::where('story_id', $story->story_id)->where('id', '<', $chapter->id)->max('id');


        $max_id = Chapter::where('story_id', $story->story_id)->orderBy('id', 'DESC')->first();
        $min_id = Chapter::where('story_id', $story->story_id)->orderBy('id', 'ASC')->first();

        // $next_chapter_slug = Chapter::where('novel_id', $novel->novel_id)->where('id', $next_chapter_id)->first();
        // $previous_chapter_slug = Chapter::where('novel_id', $novel->novel_id)->where('id', $previous_chapter_id)->first();


        return view('read_chapter', compact('chapter_image', 'story', 'all_chapter', 'chapter', 'next_chapter_id', 'previous_chapter_id', 'min_id', 'max_id'));
    }


    public function showCreateChapter($id)
    {
        $story = story::find($id);

        $datetime = Date('YmdHis');
        $countAllStory = chapter::all()->count() + 1;
        $chuoiID = $countAllStory;
        if ($countAllStory > 99)
            $chuoiID = $countAllStory;

        if ($countAllStory > 9)
            $chuoiID = '0' . $countAllStory;
        else
            $chuoiID = '00' . $countAllStory;

        $originalId = $chuoiID;
        $finalId = 'chuong' . $datetime . $originalId;

        return view('user/create_chapter', compact('story', 'finalId'));
    }

    public function addChapter(ChapterRequest $request)
    {
        $chapter = new chapter;
        $chapter->id = $request->id;
        $chapter->story_id = $request->story_id;
        $chapter->name = $request->name;
        $chapter->name_convert = $request->name_convert;
        $chapter->save();

        $image_name = $request->file('image');
        $stt = 0;
        foreach ($image_name as $image) {
            $stt = $stt + 1;
            $image->getRealPath();
            Cloudder::upload($image, null, array(
                "folder" => "$request->folder/$request->name_convert",  "overwrite" => FALSE,
                "resource_type" => "image", "responsive" => TRUE, "transformation" => array("quality" => "70", "width" => "1000", "height" => "1423", "crop" => "scale")
            ));

            $image_url = Cloudder::getResult();

            $image_chapter = new imagechapter;
            $image_chapter->name = $image_url['secure_url'];
            $image_chapter->chapter_id = $request->id;
            $image_chapter->numberical_order = $stt;
            $image_chapter->save();
        }
        $story_id = $request->story_id;
        // return redirect()->route('list-chapter,['id' => Truyen20220627060614001]'); 
        return redirect()->route('list-chapter', ['id' => $story_id]);
        // return view('user/list_chapter', compact('id_story'));

    }

    public function showEditChapter($id)
    {
        $chapter = chapter::withTrashed()->find($id);
        $chapter_image = imagechapter::with('chapter')->orderBy('numberical_order', 'ASC')->where('chapter_id', $id)->get();
        $story = story::find($chapter->story_id);
        return view('user/edit_chapter', compact('chapter', 'story', 'chapter_image'));
    }

    public function editChapter(EditChapterRequest $request, $id)
    {
        $chapter = chapter::withTrashed()->find($id);
        $chapter->name = $request->name;
        $chapter->name_convert = $request->name_convert;
        $chapter->save();

        $arrayImageID = $request->id_image;
        $i = 0;
        foreach ($arrayImageID as $value) {
            $image = imagechapter::find($value);
            $image->numberical_order = $request->numberical_order[$i];
            $image->save();
            $i++;
        }
        return redirect()->route('list-chapter', ['id' => $chapter->story_id]);
    }

    public function showCreateStory()
    {
        $category = category::orderBy('name', 'ASC')->get();

        $datetime = Date('YmdHis');
        $countAllStory = story::all()->count() + 1;
        $chuoiID = $countAllStory;
        if ($countAllStory > 99)
            $chuoiID = $countAllStory;

        if ($countAllStory > 9)
            $chuoiID = '0' . $countAllStory;
        else
            $chuoiID = '00' . $countAllStory;

        $originalId = $chuoiID;
        $finalId = 'truyen' . $datetime . $originalId;

        return view('user/create_story', compact('finalId', 'category'));
    }

    public function addNewStory(StoryRequest $request)
    {
        $story = new story;
        $story->id = $request->id;
        $story->account_id = auth()->user()->id;
        $story->name = $request->name;
        $story->name_convert = $request->name_convert;
        $story->other_name = $request->other_name;
        $story->author = $request->author;
        $story->author_convert = $request->author_convert;

        $image_name = $request->file('image');
        $image_name->getRealPath();
        Cloudder::upload($image_name, null, array(
            "folder" => "$request->name_convert",  "overwrite" => FALSE,
            "resource_type" => "image", "responsive" => TRUE, "transformation" => array("quality" => "70", "width" => "1000", "height" => "1423", "crop" => "scale")
        ));
        $image_url = Cloudder::getResult();
        $story->image = $image_url['secure_url'];
        if (empty($request->description)) {
            $story->description = "";
        } else {
            $story->description = $request->description;
        }
        $story->status_id = 1;
        $story->state = $request->state;
        $story->view = 0;
        $story->save();

        $categories = $request->category;
        foreach ($categories as $value) {
            $category = new category_of_story;
            $category->story_id = $request->id;
            $category->category_id = $value;
            $category->save();
        };
        return redirect()->route('list-story-user');
    }

    public function showEditStory($id)
    {
        $category = category::orderBy('name', 'ASC')->get();
        $story = story::with('ListStoryCategory')->where('id', $id)->first();
        $category_of_story = $story->ListStoryCategory;

        return view('user/edit_story', compact('story', 'category', 'category_of_story'));
    }
    public function editStory(EditStoryRequest $request, $id)
    {
        $story = story::find($id);
        $story->name = $request->name;
        $story->name_convert = $request->name_convert;
        $story->other_name = $request->other_name;
        $story->author = $request->author;
        $story->author_convert = $request->author_convert;

        if (!empty($request->file('image'))) {
            $image_name = $request->file('image');
            $image_name->getRealPath();
            Cloudder::upload($image_name, null, array(
                "folder" => "$request->name_convert",  "overwrite" => FALSE,
                "resource_type" => "image", "responsive" => TRUE, "transformation" => array("quality" => "70", "width" => "1000", "height" => "1423", "crop" => "scale")
            ));
            $image_url = Cloudder::getResult();
            $story->image = $image_url['secure_url'];
        }
        if (empty($request->description)) {
            $story->description = "";
        } else {
            $story->description = $request->description;
        }
        $story->status_id = 1;
        $story->state = $request->state;
        $story->ListStoryCategory()->sync($request->category);
        $story->save();

        return redirect()->route('list-story-user');
    }

    public function showEditStoryDisable($id)
    {
        $category = category::orderBy('name', 'ASC')->get();
        $story = story::onlyTrashed()->with('ListStoryCategory')->where('id', $id)->first();
        $category_of_story = $story->ListStoryCategory;
        return view('user/edit_story_disable', compact('story', 'category', 'category_of_story'));
    }
    public function editStoryDisable(Request $request, $id)
    {
        $story = story::onlyTrashed()->find($id);
        $story->name = $request->name;
        $story->name_convert = $request->name_convert;
        $story->other_name = $request->other_name;
        $story->author = $request->author;
        $story->author_convert = $request->author_convert;

        if (!empty($request->file('image'))) {
            $image_name = $request->file('image');
            $image_name->getRealPath();
            Cloudder::upload($image_name, null, array(
                "folder" => "$request->name_convert",  "overwrite" => FALSE,
                "resource_type" => "image", "responsive" => TRUE, "transformation" => array("quality" => "70", "width" => "1000", "height" => "1423", "crop" => "scale")
            ));
            $image_url = Cloudder::getResult();
            $story->image = $image_url['secure_url'];
        }
        if (empty($request->description)) {
            $story->description = "";
        } else {
            $story->description = $request->description;
        }
        $story->status_id = 1;
        $story->state = $request->state;
        $story->ListStoryCategory()->sync($request->category);
        $story->save();

        return redirect()->route('list-story-disable-user');
    }

    public function getListStoryByAuthor($slug)
    {
        $list_story = story::where('author_convert', $slug)->paginate(12);
        $list_author = story::where('author_convert', $slug)->where('status_id', 1)->first();
        $name = $list_author->author;

        return view('list_story', compact('list_story', 'name'));
    }

    public function getListStoryByCategory($slug)
    {
        //lấy ra id của tên slug
        $slug_category = category::where('name_convert', $slug)->first();
        //lấy tên của thể loại
        $name = $slug_category->name;
        //lấy ra truyện thuộc id của slug
        $story_category = category_of_story::where('category_id', $slug_category->id)->get();

        //gắn các id vào 1 mảng
        $many_category = [];
        foreach ($story_category as $story) {
            $many_category[] = $story->story_id;
        }
        //lấy các truyện từ mảng id
        $list_story = story::whereIn('id', $many_category)->where('status_id', 1)->paginate(12);


        return view('list_story', compact('list_story', 'name'));
    }

    public function deteleStory($id)
    {
        $Story = story::find($id);
        $Story->status_id = 2;
        $Story->save();
        $Story->delete();
        $favourite = favouritestory::where('story_id', $Story->id)->delete();
        report::where('story_id', $Story->id)->delete();

        $chapter = chapter::with('story')->where('story_id', $id)->delete();
        if (auth()->user()->type_account_id == 1) {
            return redirect()->route('list-story-admin');
        }
        if (auth()->user()->type_account_id == 2) {
            return redirect()->route('list-story-user');
        }
    }

    public function restoreStory($id)
    {
        $Story = story::withTrashed()->find($id);
        $Story->status_id = 1;
        $Story->save();
        $Story->restore();
        $favourite = favouritestory::where('story_id', $Story->id)->restore();
        $chapter = chapter::withTrashed()->with('story')->where('story_id', $id)->restore();
        if (auth()->user()->type_account_id == 1) {
            return redirect()->route('list-story-disable-admin');
        }
        if (auth()->user()->type_account_id == 2) {
            return redirect()->route('list-story-disable-user');
        }
    }
    public function search()
    {
        $keywords = $_GET['keywords'];
        $name = $_GET['keywords'];
        $list_story = story::where("status_id", 1)->where('name', 'LIKE', '%' . $keywords . '%')->orWhere('author', 'LIKE', '%' . $keywords . '%')->where("status_id", 1)->paginate(8);

        return view('list_story')->with(compact('name', 'list_story'));
    }

    public function favourite(Request $request)
    {
        $model = favouritestory::where($request->only('account_id', 'story_id'))->first();
        if ($model) {
            favouritestory::where($request->only('account_id', 'story_id'))->delete();
            return redirect()->back()->with('status', 'Đã xóa truyện ra khỏi danh sách yêu thích!');
        } else {
            favouritestory::create($request->only('account_id', 'story_id'));
        }
        return redirect()->back()->with('status', 'Đã thêm vào danh sách yêu thích!');
    }


    public function rating(Request $request)
    {
        $model = rate::where($request->only('story_id', 'account_id'))->first();
        if ($model) {
            rate::where($request->only('story_id', 'account_id'))
                ->update($request->only('number_star'));
        } else {
            rate::create($request->only('story_id', 'account_id', 'number_star'));
        }
        return redirect()->back()->with('status', 'Đánh giá truyện thành công!');
    }
    public function getListStoryNewChapter()
    {
        $a = chapter::with('story')->orderBy('created_at', 'DESC')->get();
        $new_chapter = $a->unique('story_id')->toQuery()->orderBy('created_at', 'DESC')->paginate(12);

        return view('list_story_new_chapter', compact('new_chapter'));
    }
    public function getListStoryCompleted()
    {
        $chapter = chapter::select(['chapter.*'])->with(['story' => function ($query) {
            $query->where([['status_id', 1], ['state', 2]])->orderBy('updated_at', 'desc');
        }])->whereHas('story', function ($query1) {
            $query1->where([['status_id', 1], ['state', 2]])->orderBy('updated_at', 'desc');
        })->orderBy('created_at', 'desc')->get();
        $complete = $chapter->unique('story_id')->toQuery()->orderBy('updated_at', 'DESC')->paginate(12);
        return view('list_story_completed', compact('complete'));
    }
    public function getListStoryNew()
    {
        $new_create = story::where('status_id', 1)->orderBy('created_at', 'desc')->paginate(12);
        return view('list_story_new', compact('new_create'));
    }
    public function getListStoryByAccount($id)
    {
        $list_story = story::where('account_id', $id)->orderBy('created_at', 'desc')->paginate(12);
        $account = account::where('id', $id)->first();
        $name = $account->name;
        return view('list_story', compact('list_story', 'name'));
    }
    public function report(Request $request)
    {
        $report = new report;
        $report->story_id = $request->story_id;
        $report->account_id = auth()->user()->id;
        $report->content = $request->content . $request->different_content;
        $report->save();
        return redirect()->back()->with('status', 'Đã báo cáo truyện này');
    }
}