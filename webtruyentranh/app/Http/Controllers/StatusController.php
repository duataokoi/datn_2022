<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\story;

class StatusController extends Controller
{
    public function changeStatusStory($id, $status_id) {
        $story = story::find($id);

       if(empty($story)) {
        echo("Story does not exist");
        return;
       }
       $story->status_id = $status_id;
       $story->save();
    }
}
