<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    use HasFactory;
    protected $table = 'comment';

    public function story(){
        return $this->belongsTo('App\Models\story');
    }
    public function account(){
        return $this->belongsTo('App\Models\account');
    }
}
