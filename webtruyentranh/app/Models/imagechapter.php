<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class imagechapter extends Model
{
    use HasFactory;
    protected $table = 'image_chapter';
    

    public function chapter(){
        return $this->belongsTo('App\Models\chapter');
    }
}
