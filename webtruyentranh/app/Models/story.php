<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class story extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'story';

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    // public function ListAccountFavourite() {
    //     return $this->belongsToMany('App\Models\account','favouritestory','account_id', 'id');
    // }

    public function favourite(){
        return $this->belongsToMany(favouritestory::class, 'favourite_story','account_id', 'story_id');
    }
    

    public function ListStoryCategory(){
        return $this->belongsToMany(category::class, 'category_of_story','story_id', 'category_id');
    }
    public function chapter(){
        return $this->hasMany('App\Models\Chapter', 'story_id', 'id');
    }
    public function commentStory(){
        return $this->hasMany('App\Models\comment', 'story_id', 'id');
    }
}
