<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class favouritestory extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'favourite_story';


    // public function Story() {
    //     return $this->belongsTo('App\Models\story');
    // }

    protected $fillable = [
        'account_id', 'story_id'
    ];

    public function story()
    {
        return $this->belongsTo(story::class, 'story_id', 'id');
    }

    public function Account()
    {
        return $this->belongsTo('App\Models\account');
    }
}