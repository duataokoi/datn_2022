<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authentication;
use Illuminate\Database\Eloquent\SoftDeletes;


class account extends Authentication
{
    use HasFactory, SoftDeletes;
    protected $table = 'account';

    public function ListStoryFavourite()
    {
        return $this->belongsToMany('App\Models\story', 'favouritestory', 'story_id', 'id');
    }
    public function commentUser(){
        return $this->hasMany('App\Models\comment', 'account_id', 'id');
    }
}