<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class chapter extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'chapter';
    protected $fillable = [
        'id ',
        'story_id ',
        'name',
        'name_convert'
    ];
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
    
    public function Status() {
        return $this->belongsTo('App\Models\status');
    }
    public function story(){
        return $this->belongsTo('App\Models\story');
    }
}
