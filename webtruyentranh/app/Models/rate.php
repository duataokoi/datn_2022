<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class rate extends Model
{
    protected $table = 'rate';
    use HasFactory;

    protected $fillable = [
        'story_id', 'account_id','number_star'
    ];

    

}
