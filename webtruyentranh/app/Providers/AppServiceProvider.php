<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\category;
use Illuminate\pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $listCategory = category::all();
        view()->composer('layouts.guess', function ($view) use ($listCategory) {
            $view->with('listCategory', $listCategory);
        });

        Paginator::useBootstrap();
    }
}