<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\StoryController;
use App\Http\Controllers\favouritestoryController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CommentController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AccountController::class, 'home'])->name('home');


Route::get('/log-in', [AccountController::class, 'viewLogin'])->name('view-log-in');
Route::post('/log-in', [AccountController::class, 'logIn'])->name('log-in');

Route::get('/sign-up', [AccountController::class, 'viewSignup'])->name('view-sign-up');
Route::post('/sign-up', [AccountController::class, 'signUp'])->name('sign-up');

Route::get('/log-out', [AccountController::class, 'logOut'])->name('log-out');
Route::get('/show', [AccountController::class, 'showToken']);

Route::get('/get-list-story', [StoryController::class, 'getListStory'])->name('get-list-story');
Route::get('/get-detail-story/{id}', [StoryController::class, 'getDetailStory'])->name('get-detail-story');
Route::get('/read-chapter/{id}', [StoryController::class, 'readChapter'])->name('read-chapter');

Route::get('/story-new-chapter', [StoryController::class, 'getListStoryNewChapter'])->name('story-new-chapter');
Route::get('/story-completed', [StoryController::class, 'getListStoryCompleted'])->name('story-completed');
Route::get('/story-new', [StoryController::class, 'getListStoryNew'])->name('story-new');

Route::get('/search', [StoryController::class, 'search'])->name('search');
//Lọc Theo Author
Route::get('/author/{slug}', [StoryController::class, 'getListStoryByAuthor'])->name('get-list-story-author');
//Lọc Theo Thể Loại
Route::get('/catelogy/{slug}', [StoryController::class, 'getListStoryByCategory'])->name('get-list-story-catelogy');

Route::get('/Account/{id}', [StoryController::class, 'getListStoryByAccount'])->name('get-list-story-account');

Route::middleware('auth')->group(function () {
    Route::get('/user', [AccountController::class, 'user'])->name('user');
    Route::get('/user-info/{id}', [AccountController::class, 'userInfo'])->name('user-info');
    Route::post('/user-info/{id}', [AccountController::class, 'changeUserInfo'])->name('change-user-info');


    Route::get('/edit-chapter/{id}', [StoryController::class, 'showEditChapter'])->name('show-edit-chapter');
    Route::post('/edit-chapter/{id}', [StoryController::class, 'editChapter'])->name('edit-chapter');

    Route::get('/list-story-user', [AccountController::class, 'listStoryByUser'])->name('list-story-user');
    Route::get('/list-story-disable-user', [AccountController::class, 'listStoryDisableByUser'])->name('list-story-disable-user');

    Route::get('/favourite-story', [AccountController::class, 'getListStoryfavourite'])->name('get-list-story-favourite');
    route::get('/delete-favourite-story/{id}', [AccountController::class, 'deleteFavouriteStory'])->name('delete-favourite-story');



    // Route::get('/add-favourite-story/{story_id}', [AccountController::class, 'addfavouriteStory'])->name('add-favourite');
    // Route::get('/get-list-story-by-author', [AccountController::class, 'getListStoryByAuthor'])->name('get-list-story-by-author');

    
    Route::post('/favourite', [StoryController::class, 'favourite'])->name('favourite');
    Route::post('/rate', [StoryController::class, 'rating'])->name('rate');

    Route::post('/save-comment', [CommentController::class, 'saveComment'])->name('save_comment');
    
    Route::post('/comment-story', [CommentController::class, 'commentStory'])->name('comment-story');
    Route::post('/delete-comment-story', [CommentController::class, 'deleteCommentStory'])->name('delete-comment-story');
    
    Route::get('/edit-story/{id}', [StoryController::class, 'showEditStory'])->name('show-edit-story');
    Route::post('/edit-story/{id}', [StoryController::class, 'editStory'])->name('edit-story');

    

    Route::get('/list-chapter/{id}', [AccountController::class, 'listChapter'])->name('list-chapter');
    Route::get('/delete-chapter/{id}', [AccountController::class, 'deleteChapter'])->name('delete-chapter');
    Route::get('/restore-chapter/{id}', [AccountController::class, 'restoreChapter'])->name('restore-chapter');

    Route::get('/create-chapter/{id}', [StoryController::class, 'showCreateChapter'])->name('create-chapter');
    Route::post('/create-chapter', [StoryController::class, 'addChapter'])->name('add-chapter');

    Route::get('/create-story', [StoryController::class, 'showCreateStory'])->name('show-create-story');
    Route::post('/add-story', [StoryController::class, 'addNewStory'])->name('add-story');


    Route::get('/edit-story-disable/{id}', [StoryController::class, 'showEditStoryDisable'])->name('show-edit-story-disable');
    Route::post('/edit-story-disable/{id}', [StoryController::class, 'editStoryDisable'])->name('edit-story-disable');

    Route::get('/list-report', [AccountController::class, 'listReport'])->name('list-report');
    Route::post('/report', [StoryController::class, 'report'])->name('report');
    Route::get('/search-story-user-login/{id}', [AccountController::class, 'searchStoryUserLogin'])->name('search-story-user-login');
    Route::get('/search-story-disable', [AccountController::class, 'searchStoryDisable'])->name('search-story-disable');
    Route::get('/search-story-favourite', [AccountController::class, 'searchStoryFavourite'])->name('search-story-favourite');
});


Route::prefix('admin')->middleware('checkadmin', 'auth')->group(function () {

    Route::get('/change-status/{id}/{status_id}', [StatusController::class, 'changeStatusStory'])->name('change-ststus-controller');

    Route::get('/list-story-admin', [AccountController::class, 'listStoryByAdmin'])->name('list-story-admin');
    Route::get('/list-story-disable-admin', [AccountController::class, 'listStoryDisableByAdmin'])->name('list-story-disable-admin');

    Route::get('/create-category', [CategoryController::class, 'showCreateCategory'])->name('show-create-category');
    Route::post('/create-category', [CategoryController::class, 'addNewCategory'])->name('create-category');
    Route::get('/list-category', [CategoryController::class, 'listCategory'])->name('list-category');
    Route::get('/show-edit-category/{id}', [CategoryController::class, 'showEditCategory'])->name('show-edit-category');
    Route::post('/edit-category/{id}', [CategoryController::class, 'editCategory'])->name('edit-category');

    Route::get('/list-account', [AccountController::class, 'listAccount'])->name('list-account');
    Route::get('/list-account-enable', [AccountController::class, 'listAccountEnable'])->name('list-account-enable');
    Route::get('/list-account-disable', [AccountController::class, 'listAccountDisable'])->name('list-account-disable');


    Route::get('/change-status-account/{id}', [AccountController::class, 'changeStatusAccount'])->name('change-status-account');

    Route::get('/search-account', [AccountController::class, 'searchEmail'])->name('search-email');

    route::get('delete-story/{id}', [StoryController::class, 'deteleStory'])->name('delete-story');
    route::get('restore-story/{id}', [StoryController::class, 'restoreStory'])->name('restore-story');

    Route::get('/delete-report/{id}', [AccountController::class, 'deleteReport'])->name('delete-report');
    route::get('delete-story-report/{id}', [AccountController::class, 'deteleStoryReport'])->name('delete-story-report');
    Route::get('/search-story-admin', [AccountController::class, 'searchStoryAdmin'])->name('search-story-admin');
});