<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\account;
use Illuminate\Support\Facades\Hash;
use App\Models\category;
use App\Models\status;
use App\Models\type_account;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Create category
        $arrayCategory = [
            "Action", "Comedy", "Drama", "Ecchi", "Fantasy", "Harem", "Horror", "Isekai", "Josei", "One shot", "Romance",
            "Seinen", "Shoujo", "Shounen", "Slice of Life", "Otome Game", "Mecha"
        ];
        $arrayCategoryConvert = [
            "action", "comedy", "drama", "ecchi", "fantasy", "harem", "horror", "isekai", "josei", "one-shot",
            "romance", "seinen", "shoujo", "shounen", "slice-of-life", "otome-game", "mecha"
        ];
        for ($i = 0; $i < count($arrayCategory); $i++) {
            $category = new category;
            $category->name = $arrayCategory[$i];
            $category->name_convert = $arrayCategoryConvert[$i];
            $category->save();
        }

        //Create status
        $statusArray = ["Enable", "Disable"];
        for ($i = 0; $i < count($statusArray); $i++) {
            $status = new status;
            $status->name = $statusArray[$i];
            $status->save();
        }

        //Create type account
        $typeAccountArray = ["Admin", "User"];
        for ($i = 0; $i < count($typeAccountArray); $i++) {
            $typeAccount = new type_account;
            $typeAccount->name = $typeAccountArray[$i];
            $typeAccount->save();
        }

        //Create account
        $name = ["Admin", "Nguyễn Tấn Lộc", "Nguyễn Huy"];
        $typeAccountID = ["1", "2", "2"];
        $email = ["admin@gmail.com", "loc@gmail.com", "huy@gmail.com"];
        $pass = "12345678";
        $avatar = "https://res.cloudinary.com/hlmanga/image/upload/v1657246248/avatar_default/avatar_lkkepx_di9pst.jpg";
        $statusID = 1;
        for ($i = 0; $i < count($name); $i++) {
            $account = new account;
            $account->name = $name[$i];
            $account->type_account_id = $typeAccountID[$i];
            $account->email = $email[$i];
            $account->password = Hash::make($pass);
            $account->avatar = $avatar;
            $account->status_id = $statusID;
            $account->save();
        }
    }
}