<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('story', function (Blueprint $table) {
            $table->string("id")->primary();
            $table->unsignedBigInteger('account_id');
            $table->foreign('account_id')->references('id')->on('account');
            $table->string('name');
            $table->string('name_convert');
            $table->string('other_name');
            $table->string('author');
            $table->string('author_convert');
            $table->string('image');
            $table->longText('description');
            $table->integer('state');
            $table->bigInteger('view')->default(0);
            $table->unsignedBigInteger('status_id');
            $table->foreign('status_id')->references('id')->on('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('story');
    }
};